# Wolf is Coming

![](https://raw.githubusercontent.com/danding0726/MachineLearning_Note/main/img/Wolf_is_Coming.png)

## Vous pouvez aussi trouver ce projet sur gitlab
https://gitlab.utc.fr/ia04_projet/ia04_final_proje.git
## Objectifs du projet

- Pour simuler la chaîne biologique commune *herbe-mouton-loup* dans les écosystèmes de prairies et observer les changements dans la chaîne alimentaire dans différentes conditions.
## Lancement de la simulation

Pour lancer la simulation, veuillez suivre ces étapes :

1. **Accéder au répertoire principal** : Ouvrez votre terminal et utilisez la commande `cd main` pour accéder au répertoire principal du projet.
2. **Démarrer le serveur principal** : Exécutez `go run main` dans le terminal pour démarrer le serveur principal. Assurez-vous que vous avez Go installé sur votre système.
3. **Accéder au répertoire de l'application web** : Ouvrez un autre terminal et changez de répertoire en utilisant la commande `cd .\web\my-react-app`. Ceci vous mènera au répertoire de l'application web.
4. **Démarrer l'application web** : Lancez l'application web en utilisant la commande `npm start`. Cette étape nécessite que Node.js et npm soient installés sur votre ordinateur.
5. **Vérification des prérequis** : Avant de démarrer, assurez-vous que vous avez le framework React installé et que le fichier `initialisation.json` est présent comme fichier de configuration modifiable.

Le format du ficher  `initialisation.json` est le suivant, et vous pouvez le modifier selon vos besoins. Merci pour faire attention: `length` et `width` sont fixés et vous ne pouvez pas les changer. 

```json
{
  "grassInitNumber": 15,
  "wolfInitNumber": 2,
  "sheepInitNumber": 10,
  "length": 30, 
  "width": 30,
  "lakeInitNumber": 2,
  "hillInitNumber": 2,
  "maxStep": 3650,
  "maxDurationSeconds": 600
}

```




## Abstract
- 1 agent 1 go routine
- 1 go routine de contrôleur pour communiquer avec des agents
- Modélisation 
  - les comportement des agents (perception,decision,action)
  - environment (weather,lake,hill)
  - interaction entre agents
  - interaction avec l'environment 
## Logique principale
1. Simulation
    1. Chaque *step* de la simulation représente une journée
2. Environment
    1. Limitées à une certaine zone, toutes les agents ne peuvent se déplacer que dans la zone
    2. Il existe différents terrains: lacs, montagnes et plaines. Les montagnes sont semblables à des obstacles. Les agents ne peuvent pas grandir sur les montagnes ni marcher dessus.
    3. Il existe deux types de météos: ensoleillé ou pluvieux. Le temps change de manière aléatoire chaque jour, ce qui affectera les perceptions et les actions des agents ainsi que le nombre de lacs.
3. Herbe(Grass)
    1. L'herbe naît au hasard sur un terrain plat
    2. La nouvelle herbe poussera après un certain cycle de croissance (le temps nécessaire varie en fonction de la météo)
    3. Lorsque l'herbe atteint son âge maximum, elle mourra automatiquement ou sera mangée par les moutons.
4. Mouton
    1. Le mouton naît au hasard sur un terrain plat
    2. Le mouton peut observer l'environnement dans une certaine cardre
    3. Si une brebis est enceinte, elle restera sur place et suspendra tout comportement normal jusqu'à ce qu'elle mette bas.Sinon,
        - Si le mouton n'observe rien, il se déplace de manière aléatoire dans l'environnement.
        - Si un mouton observe un loup, il donnera la priorité à la fuite
        - Si le mouton ne voit pas le loup et a faim, il mangera de l'herbe ; s'il n'y a pas d'herbe et qu'il a soif, il trouvera un lac pour boire de l'eau
        - Les brebis tomberont enceintes lorsqu'elles rencontreront des moutons, lorsqu'elles n'auront ni soif ni faim et qu'elles seront matures. Nouveau mouton né à côté de sa mère
    4. Le mouton meurt naturellement lorsqu'il atteit l'âge de la mort, ou est mangé par le loup, ou meurt de faim.
5. Loup
    1. Le Loup naît au hasard sur un terrain plat
    2. Le Loup peut observer l'environnement dans une certaine cardre
    3. Si une louve est enceinte, elle restera sur place et suspendra tout comportement normal jusqu'à ce qu'elle mette bas.Sinon,
        - Si le Loup n'observe rien, il se déplace de manière aléatoire dans l'environnement
        - Si un loup observe un mouton et a faim, il aura la priorité pour attraper le mouton
        - Si le loup n'observe pas les moutons et a soif, il trouvera un lac pour boire de l'eau
        - Lorsqu'une louve rencontre un loup mâle, qu'elle n'a ni faim ni soif et qu'elle est adulte, elle tombe enceinte. De nouveaux loups naissent à côté de leur mère
    4. Le loup meurt naturellement lorsqu'il atteit l'âge de la mort, ou meurt de faim.

## Conception

 ```mermaid
classDiagram
class Grass {
    -id AgentID
    +Age int
    +Age_Death int
    +Env *Environment
    +Addr Coordinates
    +GridAddr GridCoordinates
    -isDead bool
    -message chan interface
    +CircleNow int
    +GrowCircle int
}
class Mouton {
    -id AgentID
    +Age int
    +Age_Death int
    +Addr Coordinates
    +GridAddr GridCoordinates
    +Strength float32
    +Hunger_Min float32
    +Hunger_Max float32
    +ThirstLevel float32
    +Thirst_Min float32
    +Thirst_Max float32
    +LakeSeen *GridCoordinates
    +Path GridCoordinatesSlice
    +Track GridCoordinatesSlice
    -isDead bool
    -message chan interface
    +Sex string
    +Env *Environment
    +Velocity int
    +Velocity_Sunny int
    +Velocity_Rainy int
    +View int
    +View_Sunny int
    +View_Rainy int
    +Age_Adult int
    +Pregnant bool
    +BeingPregnant int
    +TimePregnant int
    +Mother *Mouton
}

class Loup {
	-id AgentID
    +Age int
    +Age_Death int
    +Addr Coordinates
    +GridAddr GridCoordinates
    +Strength float32
    +Hunger_Min float32
    +Hunger_Max float32
    +ThirstLevel float32
    +Thirst_Min float32
    +Thirst_Max float32
    +LakeSeen *GridCoordinates
    +Path GridCoordinatesSlice
    +Track GridCoordinatesSlice
    -isDead bool
    -message chan interface
    +Sex string
    +Env *Environment
    +Velocity int
    +Velocity_Sunny int
    +Velocity_Rainy int
    +View int
    +View_Sunny int
    +View_Rainy int
    +Age_Adult int
    +Pregnant bool
    +BeingPregnant int
    +TimePregnant int
    +Mother *Loup
}
 Agent <|.. Grass
 Agent <|.. Mouton
 Agent <|.. Loup
 class Agent{
        <<interface>>
        Start()
        Born(Environment)
        Grow()
        Percept(Environment)
        Decide(Environment, AgentList) AgentSlice
        Action(Environment, String, Agent) AgentSlice
        Die(Environment) bool
        ID() AgentID
        Position() Coordinates
        GridPosition() GridCoordinates
        Type() Species
        AgeNow() int
        WhatSex() string
        IsDead()
        WhatDead() bool
        AgentChannel() Channel
        UpdateGridPosition()
    }
 
 class AgentID{
 string
 }

class Coordinates{
 float32
 float32
 }

 class GridCoordinates{
 int
 int
 }

 Agent<.. AgentID 
 Agent<.. Coordinates
 Agent<.. GridCoordinates

 class Environment{
        -sync.RWMutex
        +SyncChans     sync.Map 
        +Length        int
        +Width         int
        +Agents        map
        +Lakes         GridCoordinatesSlice
        +Hills         GridCoordinatesSlice
        +GrassIDCount  int
        +LoupIDCount   int
        +MoutonIDCount int
        +GrassCount    int
        +LoupCount     int
        +MoutonCount   int
        -timepass      int
        +WeatherStatus Weather
        +LakeCount     int
        +HillCount     int
    }

Environment *-- Agent : contains

class Simulation{
        +Env         env.Environment
        -maxStep     int
        -maxDuration time.Duration
        +Mu          sync.RWMutex 
        -step        int          
        -start       time.Time
    }

Simulation<.. Environment

class RestServerAgent {
        Mutex sync.Mutex 
        Rmu sync.RWMutex 
        addr string 
        simulation Simulation 
        upgrader websocket.Upgrader 
        Start()
        AddSheepHandler()
        AddWolfHandler()
        AddGrassHandler()
        WebSocketHandler()
        getSimulationStats()
        startSimulation()
    }
 RestServerAgent<.. Simulation

class WebSocketConnection {
        String url
        open() Event
        message() Event
        error() Event
        send(Object data)
    }

class App {
        sheepCount int 
        wolfCount int
        grassCount int 
        moutonAgent Array 
        loupAgent Array 
        grassAgent Array 
        lakes Array 
        hills Array 
        lakeCount int 
        hillCount int 
        step int 
        weather Weather
        App()
        addSheep()
        addWolf()
        addGrass()
        renderLakes()
        renderHills()
        Rain()
        Sheep()
        Wolf()
        Grass() 
    }



RestServerAgent -- WebSocketConnection 
App<..  WebSocketConnection 

%% App.css is the file that provides rendering styles for the App component
 ```

