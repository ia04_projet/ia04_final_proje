package agent

import (
	"encoding/json"
	"log"
	"math/rand"
	"strconv"

	env "gitlab.utc.fr/ia04_projet/ia04_final_proje/environment"
)

type Loup struct {
	id             env.AgentID
	Age            int
	Age_Death      int
	Addr           env.Coordinates      // coordonnée
	GridAddr       env.GridCoordinates  // coordonnées de la grille
	Strength       float32              //forme du corps, satiété
	Hunger_Min     float32              //seuil de famine
	Hunger_Max     float32              //seuil de satiété maximale
	ThirstLevel    float32              //niveau de soif
	Thirst_Min     float32              //seuil du besoin de boire de l'eau minimale
	Thirst_Max     float32              //seuil du besoin de boire de l'eau maximale
	LakeSeen       *env.GridCoordinates //l'emplacement du lac vu
	Path           []env.GridCoordinates
	Track          []env.GridCoordinates
	isDead         bool
	message        chan interface{} //channel de communication avec l'environnement
	Sex            string
	Env            *env.Environment
	Velocity       int
	Velocity_Sunny int
	Velocity_Rainy int
	View           int
	View_Sunny     int
	View_Rainy     int
	Age_Adult      int  //âge de la majorité
	Pregnant       bool //enceinte ou pas
	BeingPregnant  int  //période de grossesse actuelle
	TimePregnant   int  //période de grossesse
	Mother         *Loup
}

// Initialisation du loup
func NewLoup(Env *env.Environment, message chan interface{}) *Loup {
	newLoup := Loup{
		Age:            0,
		Age_Death:      (rand.Intn(3) + 8) * 365,
		Strength:       3*rand.Float32() + 27,
		Hunger_Min:     24,
		Hunger_Max:     28,
		ThirstLevel:    5*rand.Float32() + 5,
		Thirst_Min:     10,
		Thirst_Max:     20,
		isDead:         false,
		message:        message,
		Sex:            RandSex(),
		Env:            Env,
		Velocity:       0,
		Velocity_Sunny: rand.Intn(3) + 6,
		Velocity_Rainy: rand.Intn(3) + 4,
		View:           0,
		View_Sunny:     rand.Intn(2) + 4,
		View_Rainy:     rand.Intn(2) + 3,
		Age_Adult:      30,
		Pregnant:       false,
		BeingPregnant:  0,
		TimePregnant:   20,
		Mother:         nil,
	}
	return &newLoup
}

func (l *Loup) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		ID             env.AgentID         `json:"id"`
		Age            int                 `json:"age"`
		MaxAge         int                 `json:"max_age"`
		Position       env.Coordinates     `json:"position"`
		GridPos        env.GridCoordinates `json:"grid_position"`
		Strength       float32             `json:"strength"`
		HungerMin      float32             `json:"hunger_min"`
		HungerMax      float32             `json:"hunger_max"`
		IsDead         bool                `json:"is_dead"`
		Sex            string              `json:"sex"`
		Velocity       int                 `json:"velocity"`
		Velocity_Sunny int                 `json:"velocity_Sunny"`
		Velocity_Rainy int                 `json:"velocity_Rainy"`
		View           int                 `json:"view"`
		View_Sunny     int                 `json:"view_Sunny"`
		View_Rainy     int                 `json:"view_Rainy"`
		AgeAdult       int                 `json:"age_adult"`
		Pregnant       bool                `json:"pregnant"`
	}{
		ID:             l.id,
		Age:            l.Age,
		MaxAge:         l.Age_Death,
		Position:       l.Addr,
		GridPos:        l.GridAddr,
		Strength:       l.Strength,
		HungerMin:      l.Hunger_Min,
		HungerMax:      l.Hunger_Max,
		IsDead:         l.isDead,
		Sex:            l.Sex,
		Velocity:       l.Velocity,
		Velocity_Sunny: l.Velocity_Sunny,
		Velocity_Rainy: l.Velocity_Rainy,
		View:           l.View,
		View_Sunny:     l.View_Sunny,
		View_Rainy:     l.View_Rainy,
		AgeAdult:       l.Age_Adult,
		Pregnant:       l.Pregnant,
	})
}

func RandSex() string {
	sex := []string{"F", "M"}
	index := rand.Intn(len(sex))
	chosen := sex[index]
	return chosen
}

// Mettre à jour la position de la grille
func (l *Loup) UpdateGridPosition() {
	l.Env.Lock()
	defer l.Env.Unlock()
	NewGridAddr := env.CalculateGridPosition(l.Addr)
	if l.GridAddr != NewGridAddr {
		l.Env.Agents[NewGridAddr] = append(l.Env.Agents[NewGridAddr], l)
		for nom, agent := range l.Env.Agents[l.GridAddr] {
			if agent.ID() == l.ID() {
				if len(l.Env.Agents[l.GridAddr]) == 1 {
					delete(l.Env.Agents, l.GridAddr)
					break
				} else {
					for i := nom; i < len(l.Env.Agents[l.GridAddr])-1; i++ {
						l.Env.Agents[l.GridAddr][i] = l.Env.Agents[l.GridAddr][i+1]
					}
					l.Env.Agents[l.GridAddr] = l.Env.Agents[l.GridAddr][:len(l.Env.Agents[l.GridAddr])-1]
				}
			}
		}
		l.GridAddr = NewGridAddr
	}
	l.GridAddr = NewGridAddr
}

// Ajouter du loup à l'environnement
func (l *Loup) Born(environment *env.Environment) {

	if l.Mother == nil {
		l.Addr = environment.RandomPosition()
	} else {
		l.Addr = env.Coordinates{X: l.Mother.Position().X, Y: l.Mother.Position().Y}
	}
	l.GridAddr = env.CalculateGridPosition(l.Addr)
	environment.Agents[l.GridAddr] = append(environment.Agents[l.GridAddr], l)
	environment.LoupIDCount++
	environment.LoupCount++
	id := "loup" + strconv.Itoa(environment.LoupIDCount)
	l.id = (env.AgentID)(id)
}

// La naissance d'un nouveau loup
func (l *Loup) GiveBirth(environment *env.Environment) *Loup {
	environment.Lock()
	defer environment.Unlock()
	channel := make(chan interface{})
	baby := NewLoup(environment, channel)
	baby.Mother = l
	baby.Born(environment)
	l.Pregnant = false
	l.BeingPregnant = 0
	return baby
}

func (l *Loup) Grow() {
	l.Track = []env.GridCoordinates{l.GridAddr}
	l.Age++
	l.Strength = l.Strength - 0.1
	l.ThirstLevel -= 0.1
}

// Manger du mouton
func (l *Loup) Eat(animal env.Agent, environment *env.Environment) (bool, env.Agent) {
	//Dans une certaine limite, le loup peut manger le mouton
	if animal != nil {
		x_loup := l.GridPosition().X
		y_loup := l.GridPosition().Y
		x_mouton := animal.GridPosition().X
		y_mouton := animal.GridPosition().Y
		if ((x_loup-x_mouton > -1) && (x_loup-x_mouton <= 0)) || ((x_loup-x_mouton < 1) && (x_loup-x_mouton >= 0)) {
			if ((y_loup-y_mouton > -1) && (y_loup-y_mouton <= 0)) || ((y_loup-y_mouton < 1) && (y_loup-y_mouton >= 0)) {
				animal.IsDead()
				animal.Die(environment)
				l.Strength++
				l.ThirstLevel++
				return true, animal
			}
		}
	}
	return false, nil
}

// Bois de l'eau
func (l *Loup) Drink(environment *env.Environment) bool {
	if environment.IsLake(l.GridAddr) {
		l.ThirstLevel = l.Thirst_Max
		return true
	}
	return false
}

// vu le lac
func (l *Loup) SeeLake(coor env.GridCoordinates) {
	if l.Env.IsLake(coor) {
		l.LakeSeen = &coor
	}
}

// Divers agents les plus proches de chez lui
func (l *Loup) minDistance(animals []env.Agent, kind env.Speciels) (animal_agent env.Agent, find_it bool) {
	x := l.Position().X
	y := l.Position().Y
	mindistance := float32(100)
	distance := float32(0)
	for _, animal := range animals {
		if animal.Type() == kind {
			distance = (animal.Position().X-x)*(animal.Position().X-x) + (animal.Position().Y-y)*(animal.Position().Y-y)
			if distance < mindistance {
				mindistance = distance
				animal_agent = animal
			}
			find_it = true
		} else {
			find_it = false
		}
	}
	return animal_agent, find_it
}

// Observez ce qu'il y a dans l'environnement
func (l *Loup) Percept(environment *env.Environment) (animals []env.Agent) {
	environment.Lock()
	defer environment.Unlock()

	if environment.WeatherStatus == env.SUNNY {
		l.View = l.View_Sunny
	} else {
		l.View = l.View_Rainy
	}
	x := l.GridPosition().X
	y := l.GridPosition().Y
	for i := x - l.View; i <= x+l.View; i++ {
		for j := y - l.View; j <= y+l.View; j++ {
			addr := env.GridCoordinates{X: i, Y: j}
			l.SeeLake(addr)
			ags, exists := environment.Agents[addr]
			if !exists {
				continue
			}
			for _, animal := range ags {
				if animal.Type() == env.LOUP {
					if animal.WhatSex() != l.Sex {
						animals = append(animals, animal)
					}
				} else {
					animals = append(animals, animal)
				}
			}
		}
	}
	return animals
}

func (l *Loup) Decide(environment *env.Environment, animals []env.Agent) (string, env.Agent) {
	environment.Lock()
	defer environment.Unlock()
	if l.Pregnant {
		return "Pregnant", nil
	}
	//Si le loup observe le mouton, il décide de le poursuivre
	if l.Strength < l.Hunger_Max {
		mouton, ok := l.minDistance(animals, env.MOUTON)
		if ok {
			return "Capture", mouton
		}
		//Si le loup a soif, il se rend au lac observé pour boire de l'eau.
	} else if l.ThirstLevel < l.Thirst_Min {
		if environment.IsLake(l.GridAddr) {
			return "Drink Water", nil
		} else {
			if l.LakeSeen != nil {
				return "Find Lake", nil
			}
		}
	} else if l.Strength >= l.Hunger_Max && l.ThirstLevel >= l.Thirst_Min{
		loup, ok := l.minDistance(animals, env.LOUP)
		if ok {
			//Si les deux loups sont adultes
			if l.Age >= l.Age_Adult && loup.AgeNow() >= l.Age_Adult {
				//S'il s'agit d'une louve et qu'elle rencontre un loup mâle
				if l.Sex == "F" && loup.WhatSex() == "M" {
					l.Pregnant = true
					return "Pregnant", nil
				}
			}
		}
	}
	return "Nothing special", nil
}

func (l *Loup) Action(environment *env.Environment, mess string, animal env.Agent) (string, env.Agent) {
	switch mess {
	case "Nothing special":
		l.RandomMove(environment)
		return "Nothing special", nil
	case "Capture":
		l.Capture(animal, environment)
		ok, ag := l.Eat(animal, environment)
		if ok {
			return "Eat mouton", ag
		}
		return "Capture", nil
	case "Pregnant":
		l.BeingPregnant++
		if l.BeingPregnant == l.TimePregnant {
			newbaby := l.GiveBirth(environment)
			return "GiveBirth", newbaby
		} else {
			return "Being Pregnant", nil
		}
	case "Drink Water":
		l.Drink(environment)
		return "Drink Water", nil
	case "Find Lake":
		l.Plan(environment, *l.LakeSeen)
		l.FollowPath()
		return "Find Lake", nil
	}
	return "Nothing special", nil
}

// Le traitement de la position où le loup atteint la limite de l'environnement
func (l *Loup) OutOfBound(environment *env.Environment, coordinates env.Coordinates) env.Coordinates {
	if (coordinates.X) < 0 {
		coordinates.X = 0
	} else if (coordinates.X) > float32(environment.Length) {
		coordinates.X = float32(environment.Length)
	}

	if (coordinates.Y) < 0 {
		coordinates.Y = 0
	} else if (coordinates.Y) > float32(environment.Width) {
		coordinates.Y = float32(environment.Width)
	}
	return coordinates
}

// mouvement aléatoire du loup
func (l *Loup) RandomMove(environment *env.Environment) {
	environment.Lock()
	defer environment.Unlock()
	if environment.WeatherStatus == env.SUNNY {
		l.Velocity = l.Velocity_Sunny
	} else {
		l.Velocity = l.Velocity_Rainy
	}
	randon_direction := []env.GridCoordinates{{X: -1, Y: 0}, {X: 1, Y: 0}, {X: 0, Y: -1}, {X: 0, Y: 1}, {X: -1, Y: -1}, {X: -1, Y: 1}, {X: 1, Y: -1}, {X: 1, Y: 1}}
	tempGridCoordinates := l.GridAddr
	for i := 0; i < l.Velocity; i++ {
		index := rand.Intn(8)
		direction := randon_direction[index]
		newGridCoordinates := env.GridCoordinates{X: tempGridCoordinates.X + direction.X, Y: tempGridCoordinates.Y + direction.Y}

		// Si le loup constate que l'endroit où il veut aller est un obstacle, il restera là où il se trouve et attendra la prochaine étape
		if !environment.IsWalkable(newGridCoordinates) {
			continue
		} else {
			coordinates := env.Coordinates{X: l.Addr.X + float32(direction.X), Y: l.Addr.Y + float32(direction.Y)}
			l.Addr = l.OutOfBound(environment, coordinates)
			tempGridCoordinates = newGridCoordinates // Mettez à jour la variable temporaire vers la nouvelle position de la grille
			l.Track = append(l.Track, newGridCoordinates)
		}
	}
	//m.updateGridPosition()
}

// Loup chassant les moutons
func (l *Loup) Capture(animal env.Agent, environment *env.Environment) {
	if animal != nil {
		capture_aim := env.GridCoordinates{X: animal.GridPosition().X, Y: animal.GridPosition().Y}
		l.Plan(environment, capture_aim)
		l.FollowPath()
	}
	//l.updateGridPosition()
}

// Stratégies de pistage, cherche de lac et chasse aux moutons
// //Use Dijkstra's algorithm to find the shortest path to the destination
func (l *Loup) Plan(environment *env.Environment, destination env.GridCoordinates) {
	start := l.GridPosition()
	openList := []*Node{{Coord: start, Cost: 0}}
	closedList := make(map[env.GridCoordinates]bool)

	for len(openList) > 0 {
		// Find the node with the lowest cost
		idx, current := 0, openList[0]
		for i, node := range openList {
			if node.Cost < current.Cost {
				idx, current = i, node
			}
		}

		// If we've reached the destination, we're done
		if current.Coord == destination {
			l.retracePath(current)
			return
		}

		if current.Cost > 30 {
			return
		}

		// Move the current node from the open list to the closed list
		openList = append(openList[:idx], openList[idx+1:]...)
		closedList[current.Coord] = true

		// Check all neighbors
		for _, dir := range []env.GridCoordinates{{X: -1, Y: 0}, {X: 1, Y: 0}, {X: 0, Y: -1}, {X: 0, Y: 1}, {X: -1, Y: -1}, {X: -1, Y: 1}, {X: 1, Y: -1}, {X: 1, Y: 1}} {
			neighborCoord := env.GridCoordinates{X: current.Coord.X + dir.X, Y: current.Coord.Y + dir.Y}

			// If the neighbor is not walkable or is in the closed list, skip it
			if !environment.IsWalkable(neighborCoord) || closedList[neighborCoord] {
				continue
			}

			// The cost to get to the neighbor through the current node
			cost := current.Cost + 1 // Assume all moves cost 1

			// If the neighbor is not in the open list or we've found a shorter path to it, update it
			neighbor := findNode(openList, neighborCoord)
			if neighbor == nil || cost < neighbor.Cost {
				if neighbor == nil {
					neighbor = &Node{Coord: neighborCoord}
					openList = append(openList, neighbor)
				}
				neighbor.Cost = cost
				neighbor.Parent = current
			}
		}
	}
}

func (l *Loup) retracePath(node *Node) {
	// Retrace the path from the destination to the start
	path := []env.GridCoordinates{}
	for node != nil {
		path = append([]env.GridCoordinates{node.Coord}, path...)
		node = node.Parent
	}
	l.Path = path
}

func (l *Loup) FollowPath() {
	l.Env.Lock()
	defer l.Env.Unlock()
	if l.Env.WeatherStatus == env.SUNNY {
		l.Velocity = l.Velocity_Sunny
	} else {
		l.Velocity = l.Velocity_Rainy
	}
	for i := 0; i < l.Velocity; i++ {
		if len(l.Path) > 0 {
			l.Addr = env.Coordinates{X: float32(l.Path[0].X), Y: float32(l.Path[0].Y)}
			l.Addr = l.OutOfBound(l.Env, l.Addr)
			l.Track = append(l.Track, l.Path[0])
			l.Path = l.Path[1:]
		}
	}
}

// La mort du loup
func (l *Loup) Die(environment *env.Environment) bool {
	environment.Lock()
	defer environment.Unlock()
	if l.Age >= l.Age_Death || l.isDead || l.Strength < l.Hunger_Min {
		l.isDead = true
		for nom, agent := range environment.Agents[l.GridAddr] {
			if agent.ID() == l.ID() {
				for i := nom; i < len(environment.Agents[l.GridAddr])-1; i++ {
					environment.Agents[l.GridAddr][i] = environment.Agents[l.GridAddr][i+1]
				}
				environment.Agents[l.GridAddr] = environment.Agents[l.GridAddr][:len(environment.Agents[l.GridAddr])-1]
				environment.LoupCount--
			}
		}

		return true
	}
	return false
}

func (l *Loup) ID() env.AgentID {
	return env.AgentID(l.id)
}

func (l *Loup) Position() env.Coordinates {
	return l.Addr
}

func (l *Loup) GridPosition() env.GridCoordinates {
	return l.GridAddr
}

func (l *Loup) Type() env.Speciels {
	return env.LOUP
}

func (l *Loup) AgeNow() int {
	return l.Age
}

func (l *Loup) WhatSex() string {
	return l.Sex
}

func (l *Loup) IsDead() {
	l.isDead = true
}

func (l *Loup) WhatDead() bool {
	return l.isDead
}

func (l *Loup) AgentChannel() chan interface{} {
	return l.message
}

func (l *Loup) Start() {
	log.Printf("%s start", l.ID())
	for {
		msg := <-l.message
		l.Grow()
		if !l.Die(l.Env) {
			animals := l.Percept(l.Env)
			decision, animal := l.Decide(l.Env, animals)
			// log.Printf("%s %s", l.ID(), decision)
			mess, ag := l.Action(l.Env, decision, animal)
			log.Printf("%s %s", l.ID(), mess)
			if mess == "GiveBirth" {
				l.message <- ag
			} else {
				l.message <- msg
			}
		} else {
			log.Printf("%s is dead, strength is %f ", l.ID(), l.Strength)
			l.message <- "Dead"
			return
		}
	}
}
