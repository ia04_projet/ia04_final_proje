package agent

import (
	"encoding/json"
	"log"
	"math"
	"math/rand"
	"strconv"
	"time"
	env "gitlab.utc.fr/ia04_projet/ia04_final_proje/environment"
)

type Grass struct {
	id         env.AgentID
	Age        int
	Age_Death  int //l'âge de mort de l'herbe
	Env        *env.Environment
	Addr       env.Coordinates     
	GridAddr   env.GridCoordinates 
	isDead     bool 
	message    chan interface{}
	CircleNow  int
	GrowCircle int //l'herbe pousse tous les quelques jours
}

//Initialisation de l'herbe
func (g *Grass) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		ID       env.AgentID         `json:"id"`
		Age      int                 `json:"age"`
		MaxAge   int                 `json:"age_death"`
		Position env.Coordinates     `json:"position"`
		GridPos  env.GridCoordinates `json:"grid_position"`
		IsDead   bool                `json:"is_dead"`
	}{
		ID:       g.id,
		Age:      g.Age,
		MaxAge:   g.Age_Death,
		Position: g.Addr,
		GridPos:  g.GridAddr,
		IsDead:   g.isDead,
	})
}

func NewGrass(Env *env.Environment, message chan interface{}) *Grass {
	grass := &Grass{
		Age:       0,
		Age_Death: (rand.Intn(10) + 30) , 
		Env:       Env,
		isDead:    false,
		message:   message,
		CircleNow: 0,
	}
	return grass
}

//Mettre à jour la position de la grille
func (grass *Grass) UpdateGridPosition() {
	grass.Env.Lock()
	defer grass.Env.Unlock()
	NewGridAddr := env.CalculateGridPosition(grass.Addr)
	if grass.GridAddr != NewGridAddr {
		grass.Env.Agents[NewGridAddr] = append(grass.Env.Agents[NewGridAddr], grass)
		for nom, agent := range grass.Env.Agents[grass.GridAddr] {
			if agent.ID() == grass.ID() {
				for i := nom; i < len(grass.Env.Agents[grass.GridAddr])-1; i++ {
					grass.Env.Agents[grass.GridAddr][i] = grass.Env.Agents[grass.GridAddr][i+1]
				}
				grass.Env.Agents[grass.GridAddr] = grass.Env.Agents[grass.GridAddr][:len(grass.Env.Agents[grass.GridAddr])-1]
			}
		}
	}
}

//Ajouter de l'herbe à l'environnement
func (grass *Grass) Born(environment *env.Environment) {
	grass.Addr = environment.RandomPosition()
	grass.GridAddr = env.CalculateGridPosition(grass.Addr)
	environment.Agents[grass.GridAddr] = append(environment.Agents[grass.GridAddr], grass)
	//check if the grass is near the lake
	for _, lake := range environment.Lakes {
		for _, lakeAddr := range lake {
			if math.Abs(float64(lakeAddr.X-grass.GridAddr.X)) <= 2 && math.Abs(float64(lakeAddr.Y-grass.GridAddr.Y)) <= 2 {
				grass.GrowCircle = 10
				break
			}
		}
	}
	if grass.GrowCircle == 0 {
		grass.GrowCircle = 16
	}

	environment.GrassIDCount++
	environment.GrassCount++
	id := "grass" + strconv.Itoa(environment.GrassIDCount)
	grass.id = (env.AgentID)(id)
}

func (grass *Grass) Grow() {
	grass.Age++
}

//La naissance de nouvelle herbe 
func (grass *Grass) GiveBirth(environment *env.Environment) *Grass {
	environment.Lock()
	defer environment.Unlock()
	channel := make(chan interface{})
	baby := NewGrass(environment, channel)
	baby.Born(environment)
	grass.CircleNow = 0
	return baby
}

func (grass *Grass) Percept(environment *env.Environment) (animals []env.Agent) {
	return nil
}


func (grass *Grass) Decide(environment *env.Environment, animals []env.Agent) (string, env.Agent) {
	return "", nil
}

func (grass *Grass) Action(environment *env.Environment, mess string, animal env.Agent) (string, env.Agent) {
	if environment.WeatherStatus == env.RAINY {
		//S'il pleut, l'herbe pousse plus vite
		grass.CircleNow += 2 
	} else {
		grass.CircleNow++
	}
	if grass.CircleNow >= grass.GrowCircle {
		newgrass := grass.GiveBirth(environment)
		return "GiverBirth", newgrass
	}
	return "Nothing special", nil
}

//La mort de l'herbe
func (grass *Grass) Die(environment *env.Environment) bool {
	environment.Lock()
	defer environment.Unlock()
	if grass.Age >= grass.Age_Death || grass.isDead {
		grass.isDead = true
		for nom, agent := range environment.Agents[grass.GridAddr] {
			if agent.ID() == grass.ID() {
				for i := nom; i < len(environment.Agents[grass.GridAddr])-1; i++ {
					environment.Agents[grass.GridAddr][i] = environment.Agents[grass.GridAddr][i+1]
				}
				environment.Agents[grass.GridAddr] = environment.Agents[grass.GridAddr][:len(environment.Agents[grass.GridAddr])-1]
				environment.GrassCount--
			}
		}
		return true
	}
	return false
}

func (grass *Grass) ID() env.AgentID {
	return env.AgentID(grass.id)
}

func (grass *Grass) Position() env.Coordinates {
	return grass.Addr
}

func (grass *Grass) GridPosition() env.GridCoordinates {
	return grass.GridAddr
}

func (grass *Grass) Type() env.Speciels {
	return env.GRASS
}

func (grass *Grass) AgeNow() int {
	return grass.Age
}

func (grass *Grass) WhatSex() string {
	return "Grass have not sex"
}

func (grass *Grass) IsDead() {
	grass.isDead = true
}

func (grass *Grass) WhatDead() bool {
	return grass.isDead
}

func (grass *Grass) AgentChannel() chan interface{} {
	return grass.message
}

func (grass *Grass) Start() {
	log.Printf("%s start", grass.ID())
	for {
		msg := <-grass.message
		grass.Grow()
		if !grass.Die(grass.Env){
			animals := grass.Percept(grass.Env)
			decision, animal := grass.Decide(grass.Env, animals)
			mess, ag := grass.Action(grass.Env, decision, animal)
			if mess == "GiverBirth" {
				time.Sleep(10 * time.Millisecond)
				grass.message <- ag
			} else {
				grass.message <- msg
			}
		}else {
			log.Printf("%s is dead,", grass.ID())
			grass.message <- "Dead"
			return
		}

	}
}


