package agent

import (
	"encoding/json"
	"log"
	"math/rand"
	"strconv"

	env "gitlab.utc.fr/ia04_projet/ia04_final_proje/environment"
)

type Mouton struct {
	id             env.AgentID
	Age            int
	Age_Death      int
	Addr           env.Coordinates      // coordonnée
	GridAddr       env.GridCoordinates  // coordonnées de la grille
	Strength       float32              //forme du corps, satiété
	Hunger_Min     float32              //seuil de famine
	Hunger_Max     float32              //seuil de satiété maximale
	ThirstLevel    float32              //niveau de soif
	Thirst_Min     float32              //seuil du besoin de boire de l'eau minimale
	Thirst_Max     float32              //seuil du besoin de boire de l'eau maximale
	LakeSeen       *env.GridCoordinates //l'emplacement du lac vu
	Path           []env.GridCoordinates
	Track          []env.GridCoordinates
	isDead         bool
	message        chan interface{}
	Sex            string
	Env            *env.Environment
	Velocity       int
	Velocity_Sunny int
	Velocity_Rainy int
	View           int
	View_Sunny     int
	View_Rainy     int
	Age_Adult      int  //âge de la majorité
	Pregnant       bool //enceinte ou pas
	BeingPregnant  int  //période de grossesse actuelle
	TimePregnant   int  //période de grossesse
	Mother         *Mouton
}

func (m *Mouton) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		ID             env.AgentID         `json:"id"`
		Age            int                 `json:"age"`
		MaxAge         int                 `json:"max_age"`
		Position       env.Coordinates     `json:"position"`
		GridPos        env.GridCoordinates `json:"grid_position"`
		Strength       float32             `json:"strength"`
		HungerMin      float32             `json:"hunger_min"`
		HungerMax      float32             `json:"hunger_max"`
		IsDead         bool                `json:"is_dead"`
		Sex            string              `json:"sex"`
		Velocity       int                 `json:"velocity"`
		Velocity_Sunny int                 `json:"velocity_Sunny"`
		Velocity_Rainy int                 `json:"velocity_Rainy"`
		View           int                 `json:"view"`
		View_Sunny     int                 `json:"view_Sunny"`
		View_Rainy     int                 `json:"view_Rainy"`
		AgeAdult       int                 `json:"age_adult"`
		Pregnant       bool                `json:"pregnant"`
	}{
		ID:             m.id,
		Age:            m.Age,
		MaxAge:         m.Age_Death,
		Position:       m.Addr,
		GridPos:        m.GridAddr,
		Strength:       m.Strength,
		HungerMin:      m.Hunger_Min,
		HungerMax:      m.Hunger_Max,
		IsDead:         m.isDead,
		Sex:            m.Sex,
		Velocity:       m.Velocity,
		Velocity_Sunny: m.Velocity_Sunny,
		Velocity_Rainy: m.Velocity_Rainy,
		View:           m.View,
		View_Sunny:     m.View_Sunny,
		View_Rainy:     m.View_Rainy,
		AgeAdult:       m.Age_Adult,
		Pregnant:       m.Pregnant,
	})
}

// Initialisation du mouton
func NewMouton(Env *env.Environment, message chan interface{}) *Mouton {
	newMouton := Mouton{
		Age:            0,
		Age_Death:      (rand.Intn(5) + 10) * 365,
		Strength:       4*rand.Float32() + 15,
		Hunger_Min:     13,
		Hunger_Max:     17,
		ThirstLevel:    5*rand.Float32() + 5,
		Thirst_Min:     10,
		Thirst_Max:     20,
		isDead:         false,
		message:        message,
		Sex:            RandSex(),
		Env:            Env,
		Velocity:       0,
		Velocity_Sunny: rand.Intn(3) + 3,
		Velocity_Rainy: rand.Intn(3) + 2,
		View:           0,
		View_Sunny:     rand.Intn(2) + 3,
		View_Rainy:     rand.Intn(2) + 2,
		Age_Adult:      20,
		Pregnant:       false,
		TimePregnant:   10,
		BeingPregnant:  0,
		Mother:         nil,
	}
	return &newMouton
}

// Mettre à jour la position de la grille
func (m *Mouton) UpdateGridPosition() {
	m.Env.Lock()
	defer m.Env.Unlock()
	NewGridAddr := env.CalculateGridPosition(m.Addr)
	if m.GridAddr != NewGridAddr {
		m.Env.Agents[NewGridAddr] = append(m.Env.Agents[NewGridAddr], m)
		for nom, agent := range m.Env.Agents[m.GridAddr] {
			if agent.ID() == m.ID() {
				if len(m.Env.Agents[m.GridAddr]) == 1 {
					delete(m.Env.Agents, m.GridAddr)
					break
				} else {
					for i := nom; i < len(m.Env.Agents[m.GridAddr])-1; i++ {
						m.Env.Agents[m.GridAddr][i] = m.Env.Agents[m.GridAddr][i+1]
					}
					m.Env.Agents[m.GridAddr] = m.Env.Agents[m.GridAddr][:len(m.Env.Agents[m.GridAddr])-1]
				}
			}
		}
		m.GridAddr = NewGridAddr
	}
	m.GridAddr = NewGridAddr
}

// Ajouter du mouton à l'environnement
func (m *Mouton) Born(environment *env.Environment) {
	if m.Mother == nil {
		m.Addr = environment.RandomPosition()
	} else {
		m.Addr = env.Coordinates{X: m.Mother.Position().X, Y: m.Mother.Position().Y}
	}
	m.GridAddr = env.CalculateGridPosition(m.Addr)
	environment.Agents[m.GridAddr] = append(environment.Agents[m.GridAddr], m)
	environment.MoutonIDCount++
	environment.MoutonCount++
	id := "Mouton" + strconv.Itoa(environment.MoutonIDCount)
	m.id = (env.AgentID)(id)
}

func (m *Mouton) Grow() {
	m.Track = []env.GridCoordinates{m.GridAddr}
	m.Age++
	m.Strength -= 0.1
	m.ThirstLevel -= 0.1
}

// La naissance d'un nouveau mouton
func (m *Mouton) GiveBirth(environment *env.Environment) *Mouton {
	environment.Lock()
	defer environment.Unlock()
	channel := make(chan interface{})
	baby := NewMouton(environment, channel)
	baby.Mother = m
	baby.Born(environment)
	m.Pregnant = false
	m.BeingPregnant = 0
	return baby
}

// Mouton mange de l'herbe
func (m *Mouton) Eat(grass env.Agent, environment *env.Environment) (bool, env.Agent) {
	if grass != nil {
		grass.IsDead()
		grass.Die(environment)
		m.Strength++
		//eat grass can also increase the thirst level
		m.ThirstLevel++
		return true, grass
	}
	return false, nil
}

// Bois de l'eau
func (m *Mouton) Drink(environment *env.Environment) bool {
	if environment.IsLake(m.GridAddr) {
		m.ThirstLevel = m.Thirst_Max
		return true
	}
	return false
}

// Le traitement de la position où le mouton atteint la limite de l'environnement
func (m *Mouton) OutOfBound(environment *env.Environment, coordinates env.Coordinates) env.Coordinates {
	if (coordinates.X) < 0 {
		coordinates.X = 0
	} else if (coordinates.X) > float32(environment.Length) {
		coordinates.X = float32(environment.Length)
	}

	if (coordinates.Y) < 0 {
		coordinates.Y = 0
	} else if (coordinates.Y) > float32(environment.Width) {
		coordinates.Y = float32(environment.Width)
	}
	return coordinates
}

// Le mouton échappe au loup
func (m *Mouton) Escape(animal env.Agent, environment *env.Environment) {
	if animal != nil {
		x_distance := m.Position().X - animal.Position().X
		y_distance := m.Position().Y - animal.Position().Y
		escape_aim := env.GridCoordinates{X: -1, Y: -1}
		if x_distance == 0 {
			if y_distance > 0 {
				escape_aim = env.GridCoordinates{X: m.GridAddr.X, Y: environment.Width}
			} else if y_distance < 0 {
				escape_aim = env.GridCoordinates{X: m.GridAddr.X, Y: 0}
			} else {
				return
			}
		} else if x_distance > 0 {
			if y_distance == 0 {
				escape_aim = env.GridCoordinates{X: environment.Length, Y: m.GridAddr.Y}
			} else if y_distance > 0 {
				proportion := y_distance / x_distance
				proportion_env := float32(environment.Width) / float32(environment.Length)
				if proportion < proportion_env {
					escape_aim = env.GridCoordinates{X: environment.Length, Y: int(m.Addr.Y + proportion*(float32(environment.Length)-m.Addr.X))}
				} else if proportion > proportion_env {
					escape_aim = env.GridCoordinates{X: int(m.Addr.X + (float32(environment.Width)-m.Addr.Y)/proportion), Y: environment.Width}
				} else if proportion == proportion_env {
					escape_aim = env.GridCoordinates{X: environment.Length, Y: environment.Width}
				}
			} else if y_distance < 0 {
				proportion := x_distance / (-1) * y_distance
				proportion_env := float32(environment.Length) / float32(environment.Width)
				if proportion < proportion_env {
					escape_aim = env.GridCoordinates{X: int(m.Addr.X + m.Addr.Y*proportion), Y: 0}
				} else if proportion > proportion_env {
					escape_aim = env.GridCoordinates{X: environment.Length, Y: int(m.Addr.Y - (float32(environment.Length)-m.Addr.X)/proportion)}
				} else if proportion == proportion_env {
					escape_aim = env.GridCoordinates{X: environment.Length, Y: 0}
				}

			}
		} else if x_distance < 0 {
			if y_distance == 0 {
				escape_aim = env.GridCoordinates{X: 0, Y: m.GridAddr.Y}
			} else if y_distance > 0 {
				proportion := y_distance / (-1) * x_distance
				proportion_env := float32(environment.Width) / float32(environment.Length)
				if proportion < proportion_env {
					escape_aim = env.GridCoordinates{X: 0, Y: int(m.Addr.Y + m.Addr.X*proportion)}
				} else if proportion > proportion_env {
					escape_aim = env.GridCoordinates{X: int(m.Addr.X - (float32(environment.Width)-m.Addr.Y)/proportion), Y: environment.Width}
				} else if proportion == proportion_env {
					escape_aim = env.GridCoordinates{X: 0, Y: environment.Width}
				}
			} else if y_distance < 0 {
				proportion := (-1) * x_distance / (-1) * y_distance
				proportion_env := float32(environment.Length) / float32(environment.Width)
				if proportion < proportion_env {
					escape_aim = env.GridCoordinates{X: int(m.Addr.X - m.Addr.Y*proportion), Y: 0}
				} else if proportion > proportion_env {
					escape_aim = env.GridCoordinates{X: 0, Y: int(m.Addr.Y - m.Addr.X/proportion)}
				} else if proportion == proportion_env {
					escape_aim = env.GridCoordinates{X: 0, Y: 0}
				}
			}
		}
		if escape_aim.X >= 0 && escape_aim.Y >= 0 {
			m.Plan(environment, escape_aim)
			m.FollowPath()
		}
	}
}

// mouvement aléatoire du mouton
func (m *Mouton) RandomMove(environment *env.Environment) {
	environment.Lock()
	defer environment.Unlock()
	if environment.WeatherStatus == env.SUNNY {
		m.Velocity = m.Velocity_Sunny
	} else {
		m.Velocity = m.Velocity_Rainy
	}
	randon_direction := []env.GridCoordinates{{X: -1, Y: 0}, {X: 1, Y: 0}, {X: 0, Y: -1}, {X: 0, Y: 1}, {X: -1, Y: -1}, {X: -1, Y: 1}, {X: 1, Y: -1}, {X: 1, Y: 1}}
	tempGridCoordinates := m.GridAddr
	for i := 0; i < m.Velocity; i++ {
		index := rand.Intn(8)
		direction := randon_direction[index]
		newGridCoordinates := env.GridCoordinates{X: tempGridCoordinates.X + direction.X, Y: tempGridCoordinates.Y + direction.Y}

		// Si le mouton constate que l'endroit où il veut aller est un obstacle, il restera là où il se trouve et attendra la prochaine étape
		if !environment.IsWalkable(newGridCoordinates) {
			continue
		} else {
			coordinates := env.Coordinates{X: m.Addr.X + float32(direction.X), Y: m.Addr.Y + float32(direction.Y)}
			m.Addr = m.OutOfBound(environment, coordinates)
			tempGridCoordinates = newGridCoordinates // Mettez à jour la variable temporaire vers la nouvelle position de la grille
			m.Track = append(m.Track, newGridCoordinates)
		}
	}
	//m.updateGridPosition()
}

// Divers agents les plus proches de chez lui
func (m *Mouton) minDistance(animals []env.Agent, kind env.Speciels) (animal_agent env.Agent, find_it bool) {
	x := m.Position().X
	y := m.Position().Y
	mindistance := float32(100)
	distance := float32(0)
	if animals == nil {
		return nil, false
	} else {
		for _, animal := range animals {
			if animal.Type() == kind {
				distance = (animal.Position().X-x)*(animal.Position().X-x) + (animal.Position().Y-y)*(animal.Position().Y-y)
				if distance < mindistance {
					mindistance = distance
					animal_agent = animal
				}
				find_it = true
			} else {
				find_it = false
			}
		}
		return animal_agent, find_it
	}
}

func (m *Mouton) Percept(environment *env.Environment) (animals []env.Agent) {
	environment.Lock()
	defer environment.Unlock()

	if environment.WeatherStatus == env.SUNNY {
		m.View = m.View_Sunny
	} else {
		m.View = m.View_Rainy
	}
	x := m.GridPosition().X
	y := m.GridPosition().Y
	for i := x - m.View; i <= x+m.View; i++ {
		for j := y - m.View; j <= y+m.View; j++ {
			addr := env.GridCoordinates{X: i, Y: j}
			m.SeeLake(addr)
			ags, exists := environment.Agents[addr]
			if !exists {
				continue
			}
			for _, animal := range ags {
				if animal.Type() == env.MOUTON {
					if animal.WhatSex() != m.Sex {
						animals = append(animals, animal)
					}
				} else {
					animals = append(animals, animal)
				}
			}
		}
	}
	return animals
}

// vu le lac
func (m *Mouton) SeeLake(coor env.GridCoordinates) {
	if m.Env.IsLake(coor) {
		m.LakeSeen = &coor
	}
}

type Node struct {
	Coord  env.GridCoordinates
	Cost   int
	Parent *Node
}

// Use Dijkstra's algorithm to find the shortest path
func (m *Mouton) Plan(environment *env.Environment, destination env.GridCoordinates) {
	start := m.GridPosition()
	openList := []*Node{{Coord: start, Cost: 0}}
	closedList := make(map[env.GridCoordinates]bool)

	for len(openList) > 0 {
		// Find the node with the lowest cost
		idx, current := 0, openList[0]
		for i, node := range openList {
			if node.Cost < current.Cost {
				idx, current = i, node
			}
		}

		// If we've reached the destination, we're done
		if current.Coord == destination {
			m.retracePath(current)
			return
		}

		// Move the current node from the open list to the closed list
		openList = append(openList[:idx], openList[idx+1:]...)
		closedList[current.Coord] = true

		// Check all neighbors
		for _, dir := range []env.GridCoordinates{{X: -1, Y: 0}, {X: 1, Y: 0}, {X: 0, Y: -1}, {X: 0, Y: 1}, {X: -1, Y: -1}, {X: -1, Y: 1}, {X: 1, Y: -1}, {X: 1, Y: 1}} {
			neighborCoord := env.GridCoordinates{X: current.Coord.X + dir.X, Y: current.Coord.Y + dir.Y}

			// If the neighbor is not walkable or is in the closed list, skip it
			if !environment.IsWalkable(neighborCoord) || closedList[neighborCoord] {
				continue
			}

			// The cost to get to the neighbor through the current node
			cost := current.Cost + 1 // Assume all moves cost 1

			// If the neighbor is not in the open list or we've found a shorter path to it, update it
			neighbor := findNode(openList, neighborCoord)
			if neighbor == nil || cost < neighbor.Cost {
				if neighbor == nil {
					neighbor = &Node{Coord: neighborCoord}
					openList = append(openList, neighbor)
				}
				neighbor.Cost = cost
				neighbor.Parent = current
			}
		}
	}
}

func (m *Mouton) retracePath(node *Node) {
	// Retrace the path from the destination to the start
	path := []env.GridCoordinates{}
	for node != nil {
		path = append([]env.GridCoordinates{node.Coord}, path...)
		node = node.Parent
	}
	m.Path = path
}

func findNode(nodes []*Node, coord env.GridCoordinates) *Node {
	for _, node := range nodes {
		if node.Coord == coord {
			return node
		}
	}
	return nil
}

func (m *Mouton) FollowPath() {
	m.Env.Lock()
	defer m.Env.Unlock()
	if m.Env.WeatherStatus == env.SUNNY {
		m.Velocity = m.Velocity_Sunny
	} else {
		m.Velocity = m.Velocity_Rainy
	}
	for i := 0; i < m.Velocity; i++ {
		if len(m.Path) > 0 {
			m.Addr = env.Coordinates{X: float32(m.Path[0].X), Y: float32(m.Path[0].Y)}
			m.Addr = m.OutOfBound(m.Env, m.Addr)
			m.Track = append(m.Track, m.Path[0])
			m.Path = m.Path[1:]
		}
	}
}

func (m *Mouton) Decide(environment *env.Environment, animals []env.Agent) (string, env.Agent) {
	environment.Lock()
	defer environment.Unlock()
	if m.Pregnant {
		return "Pregnant", nil
	}
	//Si un mouton voit un loup, il s'enfuira en premier
	loup, ok := m.minDistance(animals, env.LOUP)
	if ok {
		return "Escape", loup
	}
	//S'il y a de l'herbe et que mouton ne doit pas fuir les loups, mange de l'herbe
	grass, ok := m.minDistance(animals, env.GRASS)
	if ok && m.Strength < m.Hunger_Max {
		return "Eat Grass", grass
	} else if m.ThirstLevel < m.Thirst_Min {
		if environment.IsLake(m.GridAddr) {
			return "Drink Water", nil
		} else {
			if m.LakeSeen != nil {
				return "Find Lake", nil
			}
		}
	} else if m.Strength >= m.Hunger_Max && m.ThirstLevel >= m.Thirst_Min {
		mouton, ok := m.minDistance(animals, env.MOUTON)
		if ok {
			//Si les deux moutons sont adultes
			if m.Age >= m.Age_Adult && mouton.AgeNow() >= m.Age_Adult {
				if m.Sex == "F" && mouton.WhatSex() == "M" {
					m.Pregnant = true
					return "Pregnant", nil
				}
			}
		}
	}
	return "Nothing special", nil
}

func (m *Mouton) Action(environment *env.Environment, mess string, animal env.Agent) (string, env.Agent) {
	switch mess {
	case "Nothing special":
		m.RandomMove(environment)
		return "Nothing special", nil
	case "Escape":
		m.Escape(animal, environment)
		return "Escape quickly", nil
	case "Eat Grass":
		ok, ag := m.Eat(animal, environment)
		if ok {
			return "Eat grass", ag
		}
	case "Pregnant":
		m.BeingPregnant++
		if m.BeingPregnant == m.TimePregnant {
			newbaby := m.GiveBirth(environment)
			return "GiveBirth", newbaby
		} else {
			return "Being Pregnant", nil
		}
	case "Drink Water":
		m.Drink(environment)
		return "Drink Water", nil
	case "Find Lake":
		m.Plan(environment, *m.LakeSeen)
		m.FollowPath()
		return "Find Lake", nil
	}
	return "Nothing special", nil
}

// La mort du mouton
func (m *Mouton) Die(environment *env.Environment) bool {
	environment.Lock()
	defer environment.Unlock()
	if m.Age >= m.Age_Death || m.isDead || m.Strength < m.Hunger_Min {
		m.isDead = true
		for nom, agent := range environment.Agents[m.GridAddr] {
			if agent.ID() == m.ID() {
				for i := nom; i < len(environment.Agents[m.GridAddr])-1; i++ {
					environment.Agents[m.GridAddr][i] = environment.Agents[m.GridAddr][i+1]
				}
				environment.Agents[m.GridAddr] = environment.Agents[m.GridAddr][:len(environment.Agents[m.GridAddr])-1]
				environment.MoutonCount--
			}
		}
		return true
	}
	return false

}
func (m *Mouton) ID() env.AgentID {
	return env.AgentID(m.id)
}

func (m *Mouton) Position() env.Coordinates {
	return m.Addr
}

func (m *Mouton) GridPosition() env.GridCoordinates {
	return m.GridAddr
}

func (m *Mouton) Type() env.Speciels {
	return env.MOUTON
}

func (m *Mouton) AgeNow() int {
	return m.Age
}

func (m *Mouton) WhatSex() string {
	return m.Sex
}

func (m *Mouton) IsDead() {
	m.isDead = true
}

func (m *Mouton) WhatDead() bool {
	return m.isDead
}

func (m *Mouton) AgentChannel() chan interface{} {
	return m.message
}

func (m *Mouton) Start() {
	log.Printf("%s start", m.ID())
	for {
		msg := <-m.message
		m.Grow()
		if !m.Die(m.Env) {
			animals := m.Percept(m.Env)
			decision, animal := m.Decide(m.Env, animals)
			log.Printf("%s %s", m.ID(), decision)
			mess, ag := m.Action(m.Env, decision, animal)

			if mess == "GiveBirth" {
				m.message <- ag
			} else {
				m.message <- msg
			}
		} else {
			log.Printf("%s is dead, strength is %f", m.ID(), m.Strength)
			m.message <- "Dead"
			return
		}
	}
}
