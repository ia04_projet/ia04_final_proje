package environment

import "math/rand"

//Init Landform of the environment
func (env *Environment) InitLandform(lakeCount int, hillCount int) {
	env.Lakes = make([][]GridCoordinates, lakeCount)
	env.Hills = make([][]GridCoordinates, hillCount)
	lakeSize := rand.Intn(5) + 1
	hillSize := rand.Intn(5) + 1
	//Create Square lake
	for i := 0; i < lakeCount; i++ {
		LakeX := rand.Intn(env.Length - lakeSize)
		LakeY := rand.Intn(env.Width - lakeSize)
		for j := LakeX; j < LakeX+lakeSize; j++ {
			for k := LakeY; k < LakeY+lakeSize; k++ {
				env.Lakes[i] = append(env.Lakes[i], GridCoordinates{X: j, Y: k})
			}
		}
	}
	//Create Square hill
	for i := 0; i < hillCount; i++ {
		hillX := rand.Intn(env.Length - hillSize)
		hillY := rand.Intn(env.Width - hillSize)
		for j := hillX; j < hillX+hillSize; j++ {
			for k := hillY; k < hillY+hillSize; k++ {
				if !env.IsLake(GridCoordinates{X: j, Y: k}) {
					env.Hills[i] = append(env.Hills[i], GridCoordinates{X: j, Y: k})
				}
			}
		}
	}
}

//Delete a lake
func (env *Environment) DeleteLake() {
	env.Lock()
	defer env.Unlock()
	if len(env.Lakes) > 0{
		env.Lakes = env.Lakes[:len(env.Lakes)-1]
		env.LakeCount--
	}
}

//Add a lake
func (env *Environment) AddLake() {
	env.Lock()
	defer env.Unlock()
	lakeSize := rand.Intn(5) + 1
	LakeX := rand.Intn(env.Length - lakeSize)
	LakeY := rand.Intn(env.Width - lakeSize)
	new_lake := make([]GridCoordinates,0)
	for j := LakeX; j < LakeX+lakeSize; j++ {
		for k := LakeY; k < LakeY+lakeSize; k++ {
			gridLake := GridCoordinates{X: j, Y: k}
			if !env.IsLake(gridLake) || !env.IsHill(gridLake) {
				new_lake = append(new_lake, gridLake)
			}
		}
	}
	if len(new_lake) > 0 {
		env.Lakes = append(env.Lakes, new_lake)
		env.LakeCount++
	}
}

//If a GirdCoordinate in the lake
func (env *Environment) IsLake(addr GridCoordinates) bool {
	for _, lake := range env.Lakes {
		for _, lakeAddr := range lake {
			if addr == lakeAddr {
				return true
			}
		}
	}
	return false
}

//If a GirdCoordinate in the hill
func (env *Environment) IsHill(addr GridCoordinates) bool {
	for _, hill := range env.Hills {
		for _, hillAddr := range hill {
			if addr == hillAddr {
				return true
			}
		}
	}
	return false
}

//Get the GirdCoordinate of Plain 
func (env *Environment) GetPlain() []GridCoordinates {
	var plain []GridCoordinates
	for i := 0; i < env.Length; i++ {
		for j := 0; j < env.Width; j++ {
			addr := GridCoordinates{X: i, Y: j}
			if !env.IsLake(addr) && !env.IsHill(addr) {
				plain = append(plain, addr)
			}
		}
	}
	return plain
}

func (env *Environment) IsWalkable(coord GridCoordinates) bool {
	if env.IsHill(coord) {
		return false
	}
	if coord.X < 0 || coord.X >= env.Length || coord.Y < 0 || coord.Y >= env.Width {
		return false
	}
	return true
}
