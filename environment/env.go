package environment

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"sync"
	// "log"
	// "time"
)

// 网格坐标
type GridCoordinates struct {
	X int
	Y int
}

type Coordinates struct {
	X float32
	Y float32
}

type Speciels int
type Weather int
type landform int

const (
	GRASS Speciels = iota
	MOUTON
	LOUP
)

const (
	SUNNY Weather = iota
	RAINY
)

const (
	PLAIN landform = iota
	LAKE
	HILL
)

type Agent interface {
	Start()
	Born(*Environment)
	Grow()
	Percept(*Environment) []Agent
	Decide(*Environment, []Agent) (string, Agent)
	Action(*Environment, string, Agent) (string, Agent)
	Die(*Environment) bool
	ID() AgentID
	Position() Coordinates
	GridPosition() GridCoordinates
	Type() Speciels
	AgeNow() int
	WhatSex() string
	IsDead()
	WhatDead() bool
	AgentChannel() chan interface{}
	UpdateGridPosition()
}

type AgentID string
type AgentAge int

type Environment struct {
	sync.RWMutex
	SyncChans     sync.Map //communiquer avec les goroutines d'agents
	Length        int
	Width         int
	Agents        map[GridCoordinates][]Agent 
	Lakes         [][]GridCoordinates
	Hills         [][]GridCoordinates
	GrassIDCount  int
	LoupIDCount   int
	MoutonIDCount int
	GrassCount    int
	LoupCount     int
	MoutonCount   int
	timepass      int
	WeatherStatus Weather
	LakeCount     int
	HillCount     int
}

func (env *Environment) MarshalJSON() ([]byte, error) {
	type Alias Environment
	return json.Marshal(&struct {
		Agents map[string][]json.RawMessage `json:"agents"`
		*Alias
	}{
		Agents: convertAgentsMapToJSON(env.Agents),
		Alias:  (*Alias)(env),
	})
}

func (gc GridCoordinates) String() string {
	return fmt.Sprintf("%d,%d", gc.X, gc.Y)
}

func convertAgentsMapToJSON(agents map[GridCoordinates][]Agent) map[string][]json.RawMessage {
	agentMap := make(map[string]Agent)
	for _, agent := range agents {
		for _, ag := range agent {
			agentMap[string(ag.ID())] = ag
		}
	}
	jsonMap := make(map[string][]json.RawMessage)
	for _, agent := range agentMap {
		position := agent.GridPosition()
		key := position.String()
		agentJSON, _ := json.Marshal(agent)
		jsonMap[key] = append(jsonMap[key], json.RawMessage(agentJSON))
	}
	return jsonMap
}

// Init the environment
func NewEnvironment(length int, width int, grassCount int, loupCount int, moutonCount int, lakeCount int, hillCount int) *Environment {
	env := &Environment{
		Length:        length,
		Width:         width,
		Agents:        make(map[GridCoordinates][]Agent),
		GrassIDCount:  grassCount,
		LoupIDCount:   loupCount,
		MoutonIDCount: moutonCount,
		GrassCount:    grassCount,
		LoupCount:     loupCount,
		MoutonCount:   moutonCount,
		timepass:      0,
		WeatherStatus: SUNNY,
		LakeCount:     lakeCount,
		HillCount:     hillCount,
	}
	env.InitLandform(lakeCount, hillCount)
	return env
}

func (env *Environment) RandomWeather() Weather {
	w := rand.Intn(10)
	if w < 4 {
		return RAINY
	}
	return SUNNY
}

// Add an agent to the environment
func (env *Environment) AddAgent(agent Agent) {
	env.Lock()
	defer env.Unlock()
	agent.Born(env)
}

// Init Agents Positions in the environment (only in plain)
func (env *Environment) RandomPosition() Coordinates {
	coor := Coordinates{X: (float32(env.Length)) * rand.Float32(), Y: (float32(env.Width)) * rand.Float32()}
	for env.IsLake(CalculateGridPosition(coor)) || env.IsHill(CalculateGridPosition(coor)) {
		coor = Coordinates{X: (float32(env.Length)) * rand.Float32(), Y: (float32(env.Width)) * rand.Float32()}
	}
	return coor
}

// CalculateGridPosition 
func CalculateGridPosition(pos Coordinates) GridCoordinates {
	return GridCoordinates{X: int(pos.X), Y: int(pos.Y)}
}

