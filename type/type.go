package TYPE

import (
	ag "gitlab.utc.fr/ia04_projet/ia04_final_proje/agent"
)

type SimulationStatsResponse struct {
	SheepCount   int         `json:"sheepCount"`
	WolfCount    int         `json:"wolfCount"`
	GrassCount   int         `json:"grassCount"`
	StepCount    int         `json:"stepCount"`
	GrassAgents  []ag.Grass  `json:"grassAgent"`
	MoutonAgents []ag.Mouton `json:"moutonAgent"`
	LoupAgents   []ag.Loup   `json:"loupAgent"`
}
