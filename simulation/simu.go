package simulation

import (
	"log"
	"sync"

	// "sync"
	"time"

	ag "gitlab.utc.fr/ia04_projet/ia04_final_proje/agent"
	env "gitlab.utc.fr/ia04_projet/ia04_final_proje/environment"
)

type Simulation struct {
	Env         env.Environment
	maxStep     int
	maxDuration time.Duration
	Mu          sync.RWMutex // Ce mutex sera utilisé pour synchroniser l'accès à la map Agents
	step        int          // Stats
	start       time.Time
}

func NewSimulation(GrassInitNumber int, LoupInitNumber int, MoutonInitNumber int, length int, width int, LakeInitNumber int, HillInitNumber int, maxStep int, maxDuration time.Duration) *Simulation {
	sim := &Simulation{
		Env:         *env.NewEnvironment(length, width, 0, 0, 0, LakeInitNumber, HillInitNumber),
		maxStep:     maxStep,
		maxDuration: maxDuration,
		step:        0,
	}
	for i := 0; i < GrassInitNumber; i++ {
		syncChan := make(chan interface{})
		grass := ag.NewGrass(&sim.Env, syncChan)
		sim.Env.AddAgent(grass)
		sim.Env.SyncChans.Store(grass.ID(), syncChan)
	}
	for i := 0; i < LoupInitNumber; i++ {
		syncChan := make(chan interface{})
		loup := ag.NewLoup(&sim.Env, syncChan)
		sim.Env.AddAgent(loup)
		sim.Env.SyncChans.Store(loup.ID(), syncChan)
	}
	for i := 0; i < MoutonInitNumber; i++ {
		syncChan := make(chan interface{})
		mouton := ag.NewMouton(&sim.Env, syncChan)
		sim.Env.AddAgent(mouton)
		sim.Env.SyncChans.Store(mouton.ID(), syncChan)
	}
	return sim
}

func (sim *Simulation) Run() {
	//
	// sim.Manager.RLock()
	// defer sim.Manager.RUnlock()
	//
	sim.start = time.Now()
	ticker := time.NewTicker(3 * time.Second)
	agents_in_map := sim.Env.Agents

	for _, agents := range agents_in_map {
		for _, agent := range agents {
			go agent.Start()
		}
	}
	time.Sleep(time.Second)
	go func() {
		weather_count := 0
		for sim.step < sim.maxStep {
			for range ticker.C {
				//Météo générée aléatoirement
				weather := sim.Env.RandomWeather()
				sim.Env.WeatherStatus = weather
				if sim.Env.WeatherStatus == env.SUNNY {
					weather_count++
				} else if sim.Env.WeatherStatus == env.RAINY {
					weather_count--
				}
				if weather_count > 15 {
					//S'il y a trop de jours ensoleillés, le lac va s'assécher
					sim.Env.DeleteLake()
					weather_count = 0
				} else if weather_count < -5 {
					//S'il pleut davantage, de nouveau lac apparaîtront
					sim.Env.AddLake()
					weather_count = 0
				}
				log.Printf("Step:%d,Weather:%d", sim.step, sim.Env.WeatherStatus)
				log.Printf("Grass: %d, Loup: %d, Mouton: %d", sim.Env.GrassCount, sim.Env.LoupCount, sim.Env.MoutonCount)
				for _, agents := range agents_in_map {
					for _, agent := range agents {
						if agent.WhatDead() {
							continue
						} else {
							c, _ := sim.Env.SyncChans.Load(agent.ID())
							// for !ok {
							// 	log.Printf("Agent %s not found in syncChan", agent.ID())
							// 	if agent.AgentChannel() != nil{
							// 		log.Printf("Exist AgentChannel")
							// 	}
							// 	sim.Env.SyncChans.Store(agent.ID(), agent.AgentChannel())
							// 	time.Sleep(50 * time.Millisecond)
							// 	c, ok = sim.Env.SyncChans.Load(agent.ID())
							// }
							//Envoyer un message au goroutine d'agent
							c.(chan interface{}) <- sim.step
							time.Sleep(20 * time.Millisecond)
							//Accepter les messages du goroutine d'agent
							mess := <-c.(chan interface{})
							//Si le message reçu est un nouvel agent
							ag, fine := mess.(env.Agent)
							if fine {
								syncChan := ag.AgentChannel()
								sim.Env.SyncChans.Store(ag.ID(), syncChan)
								go ag.Start()
							}
						}
					}
				}
				for _, agents := range agents_in_map {
					for _, agent := range agents {
						agent.UpdateGridPosition()
					}
				}
				agents_in_map = sim.Env.Agents
				sim.step++
			}
		}
	}()

	time.Sleep(sim.maxDuration)
	log.Printf("Fin de la simulation [step: %d, GrassCount: %d, LoupCount: %d, MoutonCount: %d]", sim.step, sim.Env.GrassCount, sim.Env.LoupCount, sim.Env.MoutonCount)
}

func (sim *Simulation) GetAgents() (int, int, int, int, []*ag.Grass, []*ag.Mouton, []*ag.Loup) {
	sim.Env.RWMutex.Lock()
	defer sim.Env.RWMutex.Unlock()

	var grassAgents []*ag.Grass
	var moutonAgents []*ag.Mouton
	var loupAgents []*ag.Loup

	for _, spec := range sim.Env.Agents {
		for _, agent := range spec {
			switch a := agent.(type) {
			case *ag.Grass:
				grassAgents = append(grassAgents, a)
			case *ag.Mouton:
				moutonAgents = append(moutonAgents, a)
			case *ag.Loup:
				loupAgents = append(loupAgents, a)
			}
		}
	}

	return sim.Env.GrassCount, sim.Env.MoutonCount, sim.Env.LoupCount, sim.step, grassAgents, moutonAgents, loupAgents
}
