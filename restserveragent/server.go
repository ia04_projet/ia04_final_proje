package restserveragent

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	ag "gitlab.utc.fr/ia04_projet/ia04_final_proje/agent"
	env "gitlab.utc.fr/ia04_projet/ia04_final_proje/environment"
	simu "gitlab.utc.fr/ia04_projet/ia04_final_proje/simulation"
)

type RestServerAgent struct {
	sync.Mutex // contrôle run et getSimulationStats
	Rmu        sync.RWMutex
	addr       string
	simulation *simu.Simulation
	upgrader   websocket.Upgrader
}

type Config struct {
	GrassInitNumber    int `json:"grassInitNumber"`
	WolfInitNumber     int `json:"wolfInitNumber"`
	SheepInitNumber    int `json:"sheepInitNumber"`
	Length             int `json:"length"`
	Width              int `json:"width"`
	LakeInitNumber     int `json:"lakeInitNumber"`
	HillInitNumber     int `json:"hillInitNumber"`
	MaxStep            int `json:"maxStep"`
	MaxDurationSeconds int `json:"maxDurationSeconds"`
}

func LoadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var config Config
	if err := json.NewDecoder(file).Decode(&config); err != nil {
		return nil, err
	}

	return &config, nil
}

type SimulationParams struct {
	GrassInitNumber  int           `json:"grassInitNumber"`
	LoupInitNumber   int           `json:"loupInitNumber"`
	MoutonInitNumber int           `json:"moutonInitNumber"`
	Length           int           `json:"length"`
	Width            int           `json:"width"`
	MaxStep          int           `json:"maxstep"`
	MaxDuration      time.Duration `json:"maxDuration"`
}

type WSMessage struct {
	Type string `json:"type"`
	//Params SimulationParams `json:"params"`
}

func NewRestServerAgent(addr string) *RestServerAgent {
	return &RestServerAgent{
		addr: addr,
		upgrader: websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
			CheckOrigin: func(r *http.Request) bool {
				return r.Header.Get("Origin") == "http://localhost:3000"
			},
		},
	}
}

func (rsa *RestServerAgent) startSimulation() {
	config, err := LoadConfig("initialisation.json")
	if err != nil {
		log.Fatalf("Failed to load config: %v", err)
	}
	rsa.simulation = simu.NewSimulation(
		config.GrassInitNumber,
		config.WolfInitNumber,
		config.SheepInitNumber,
		config.Length,
		config.Width,
		config.LakeInitNumber,
		config.HillInitNumber,
		config.MaxStep,
		time.Duration(config.MaxDurationSeconds)*time.Second,
	)
	go rsa.simulation.Run()
}

func (rsa *RestServerAgent) WebSocketHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := rsa.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("upgrade error:", err)
		return
	}
	defer conn.Close()

	ticker := time.NewTicker(50 * time.Millisecond)
	defer ticker.Stop()

	// Utilisez readMessages pour lire les messages
	msgChan := readMessages(conn)

	for {
		select {
		case <-ticker.C:
			if rsa.simulation != nil {
				stats := rsa.getSimulationStats()
				if err := conn.WriteJSON(stats); err != nil {
					log.Println("write error:", err)
					return
				}
			}

		case msg, ok := <-msgChan:
			if !ok {
				return
			}
			if msg.Type == "start_simulation" {
				rsa.startSimulation()
				if err := conn.WriteMessage(websocket.TextMessage, []byte("Simulation started")); err != nil {
					log.Println("write error:", err)
					return
				}
			}
		}
	}
}

func readMessages(conn *websocket.Conn) <-chan WSMessage {
	msgChan := make(chan WSMessage)
	go func() {
		defer close(msgChan)
		for {
			var msg WSMessage
			err := conn.ReadJSON(&msg)
			if err != nil {
				if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
					log.Printf("WebSocket connection closed unexpectedly: %v", err)
				} else if err == websocket.ErrCloseSent {
					log.Println("WebSocket close frame sent, stopping read loop")
					return
				} else {
					log.Printf("read error: %v", err)
				}
				break // ou retour, selon votre logique prévue
			}
			msgChan <- msg
		}
	}()
	return msgChan
}

func (rsa *RestServerAgent) getSimulationStats() interface{} {
	rsa.simulation.Mu.Lock()         // Verrouillez les ressources partagées avant d'y accéder
	defer rsa.simulation.Mu.Unlock() // Assurez-vous que le verrou est libéré lorsque la fonction se termine
	if rsa.simulation == nil {
		return nil
	}

	// Obtenez toutes les données directement depuis GetAgents
	grassCount, moutonCount, loupCount, step, grassAgents, moutonAgents, loupAgents := rsa.simulation.GetAgents()
	lakes, hills := rsa.simulation.Env.Lakes, rsa.simulation.Env.Hills
	Lakescount, Hillcount := rsa.simulation.Env.LakeCount, rsa.simulation.Env.HillCount
	weather := rsa.simulation.Env.WeatherStatus

	// Utilisez les données obtenues pour construire la structure des statistiques
	stats := struct {
		SheepCount   int                     `json:"sheepCount"`
		WolfCount    int                     `json:"wolfCount"`
		GrassCount   int                     `json:"grassCount"`
		Step         int                     `json:"step"`
		GrassAgents  []*ag.Grass             `json:"grassAgent"`
		MoutonAgents []*ag.Mouton            `json:"moutonAgent"`
		LoupAgents   []*ag.Loup              `json:"loupAgent"`
		LakeCount    int                     `json:"lakeCount"`
		HillCount    int                     `json:"hillCount"`
		Lakes        [][]env.GridCoordinates `json:"lakes"`
		Hills        [][]env.GridCoordinates `json:"hills"`
		Weather      env.Weather             `json:"weather"`
	}{
		SheepCount:   moutonCount,
		WolfCount:    loupCount,
		GrassCount:   grassCount,
		Step:         step,
		GrassAgents:  grassAgents,
		MoutonAgents: moutonAgents,
		LoupAgents:   loupAgents,
		LakeCount:    Lakescount,
		HillCount:    Hillcount,
		Lakes:        lakes,
		Hills:        hills,
		Weather:      weather,
	}

	//log.Printf("Sending stats: %+v", stats) // Sortie des données envoyées
	return stats
}

func (rsa *RestServerAgent) AddSheepHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	rsa.Lock()
	if rsa.simulation == nil {
		rsa.Unlock()
		http.Error(w, "Simulation not started", http.StatusConflict)
		return
	}
	env := &rsa.simulation.Env
	rsa.Unlock()

	syncChan := make(chan interface{})
	mouton := ag.NewMouton(env, syncChan)

	env.AddAgent(mouton)
	env.Lock()
	env.SyncChans.Store(mouton.ID(), syncChan)
	env.Unlock()
	go mouton.Start()

}

func (rsa *RestServerAgent) AddWolfHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	rsa.Lock()
	if rsa.simulation == nil {
		rsa.Unlock()
		http.Error(w, "Simulation not started", http.StatusConflict)
		return
	}
	env := &rsa.simulation.Env
	rsa.Unlock()

	syncChan := make(chan interface{})
	loup := ag.NewLoup(env, syncChan)

	env.AddAgent(loup)
	env.Lock()
	env.SyncChans.Store(loup.ID(), syncChan)
	env.Unlock()
	go loup.Start()

}

func (rsa *RestServerAgent) AddGrassHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	rsa.Lock()
	if rsa.simulation == nil {
		rsa.Unlock()
		http.Error(w, "Simulation not started", http.StatusConflict)
		return
	}
	env := &rsa.simulation.Env
	rsa.Unlock()

	syncChan := make(chan interface{})
	grass := ag.NewGrass(env, syncChan)

	env.AddAgent(grass)
	env.Lock()
	env.SyncChans.Store(grass.ID(), syncChan)
	env.Unlock()
	go grass.Start()

}

func (rsa *RestServerAgent) Start() {
	mux := http.NewServeMux()
	mux.HandleFunc("/ws", rsa.WebSocketHandler)
	mux.HandleFunc("/add-sheep", rsa.AddSheepHandler)
	mux.HandleFunc("/add-wolf", rsa.AddWolfHandler)
	mux.HandleFunc("/add-grass", rsa.AddGrassHandler)

	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Println("Listening on", rsa.addr)
	log.Fatal(s.ListenAndServe())
}
