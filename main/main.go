package main

import (
	"os"

	"gitlab.utc.fr/ia04_projet/ia04_final_proje/restserveragent"
)

func main() {
	//s := simulation.NewSimulation(10, 0, 0, 50, 50, 2, 2, 365, 600*time.Second)
	//#s.Run() //go s.Run()
	var addr string
	if len(os.Args) > 1 {
		addr = os.Args[1]
	} else {
		addr = ":3030"
	}
	rsa := restserveragent.NewRestServerAgent(addr)

	rsa.Start()
}
