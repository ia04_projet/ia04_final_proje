import React, { useState, useEffect } from "react";
import { useRef } from "react";
import { useAnimation } from "framer-motion";
// import axios from "axios";
import { motion } from "framer-motion";
import './App.css';

 // Create WebSocket connection.
 const socket = new WebSocket('ws://localhost:3030/ws'); // Replace with the correct URL

 // Connection opened
 
 socket.addEventListener('open', function (event) {
   console.log('Connected to WS Server');
   // Send "start_simulation" message to the server
   socket.send(JSON.stringify({ Type: "start_simulation" }));
 });

 const SCALE = 40; // Adjust this scale based on your simulation-to-display mapping

 const getStyle = (GridPos) => {
    // Calculate the pixel position based on the simulation coordinates
    var left = GridPos.X * SCALE+20;
    var top = GridPos.Y * SCALE+20;
    return {
      position: 'absolute',
      left: `${left}px`,
      top: `${top}px`,
      transform: 'translate(-50%, -50%)', // Centers the image on the coordinate
    };
  
  };


  const Rain = () => {
    const raindrops = new Array(150).fill(null).map((_, index) => (
        <div key={index} className="raindrop" style={{ 
            left: `${Math.random() * 100}vw`,
            animationDuration: `${Math.random() * 0.6 + 0.6}s`
        }} />
    ));

    return <div className="rain-container">
      {raindrops}
      <audio src="rain.wav" autoPlay loop />
    </div>;
};
  

function App() {
  // Define state variables for each type of agent
  const [sheepCount, setSheepCount] = useState(0);
  const [wolfCount, setWolfCount] = useState(0);
  const [grassCount, setGrassCount] = useState(0);
  const [moutonAgent, setSheepAgent] = useState([]);
  const [loupAgent, setWolfAgent] = useState([]);
  const [grassAgent, setGrassAgent] = useState([]);

  const [lakes, setLakes] = useState([]);
  const [hills, setHills] = useState([]);
  const [lakeCount, setLakeCount] = useState(0);
  const [hillCount, setHillCount] = useState(0);
  
  const [step, setStep] = useState(0)
  
  const [weather, setWeather] = useState(0)

  const gridSize = 31;
  const cells = [];
  for (let i = 0; i < gridSize * gridSize; i++) {
    cells.push(<div className="grid-cell" key={i}></div>);
  }

  
  useEffect(() => {
        // Listen for messages
      socket.addEventListener('message', function (event) {
        //console.log('Message from server ', event.data);
        try {
          const receivedData = JSON.parse(event.data);
          //console.log('Parsed data', receivedData); // 
          // Assuming the server sends data in the following format:
          // { sheepCount: X, wolfCount: Y, grassCount: Z, grassAgents: [...], moutonAgents: [...], loupAgents: [...] }
          // Update state variables accordingly
          setSheepCount(receivedData.sheepCount);
          setWolfCount(receivedData.wolfCount);
          setGrassCount(receivedData.grassCount);
          setGrassAgent(receivedData.grassAgent);
          setSheepAgent(receivedData.moutonAgent);
          setWolfAgent(receivedData.loupAgent);
          setLakes(receivedData.lakes);
          setHills(receivedData.hills);
          setLakeCount(receivedData.lakeCount);
          setHillCount(receivedData.hillCount);
          setWeather(receivedData.weather);
          setStep(receivedData.step);
        } catch (error) {
          console.error('Error parsing message data:', error);
        }
      });


      // Listen for possible errors
      socket.addEventListener('error', function (event) {
        console.error('WebSocket error observed:', event);
      });


}, []);
  console.log(grassAgent)
    
  
  const addSheep = async () => {
    try {
      const response = await fetch('http://localhost:3030/add-sheep', {
        method: 'POST',
      });
      if (response.ok) {
        console.log('Sheep added');
      } else {
        console.error('Failed to add sheep');
      }
    } catch (error) {
      console.error('Error adding sheep:', error);
    }
  };

  const addWolf = async () => {
    try {
      const response = await fetch('http://localhost:3030/add-wolf', {
        method: 'POST',
      });
      if (response.ok) {
        console.log('Wolf added');
      } else {
        console.error('Failed to add wolf');
      }
    } catch (error) {
      console.error('Error adding wolf:', error);
    }
  };

  const addGrass = async () => {
    try {
      const response = await fetch('http://localhost:3030/add-grass', {
        method: 'POST',
      });
      if (response.ok) {
        console.log('Grass added');
      } else {
        console.error('Failed to add grass');
      }
    } catch (error) {
      console.error('Error adding grass:', error);
    }
  };
  
  // Define the functions for rendering lakes and hills
  
  const renderLakes = () => {
    // Check if "lakes" is null or not an array
    if (!Array.isArray(lakes)) return null;
  
    return lakes.map((lake, index) => (
      // Check if each lake is null or not an array
      Array.isArray(lake) && lake.map((coordinate, idx) => (
        <img
          key={`lake-${index}-${idx}`}
          src="river2.jpg" 
          alt="Lake"
          className="lake-cell"
          style={{
            position: 'absolute',
            left: `${coordinate.X * 40}px`,
            top: `${coordinate.Y * 40}px`,
            width: '40px',
            height: '40px'
          }}
        />
      ))
    ));
  };
  

  const renderHills = () => {
    // Check if "hills" is null or not an array
    if (!Array.isArray(hills)) return null;
  
    return hills.map((hill, index) => (
      // Check if each hill is null or not an array
      Array.isArray(hill) && hill.map((coordinate, idx) => (
        <img
          key={`hill-${index}-${idx}`}
          src="hill2.png" 
          alt="Hill"
          className="hill-cell"
          style={{
            position: 'absolute', 
            left: `${coordinate.X * 40}px`,
            top: `${coordinate.Y * 40}px`,
            width: '40px',
            height: '40px'
          }}
        />
      ))
    ));
  };
  
  



  

  return (
      <div className="App">
        <div className="App-header">
          <h1>Ecosystem Simulation</h1>
        </div>
        {/* If it is a rainy day, render the "Rain" component/function */}
        {weather !== 0 && <Rain />} 
        <div className="container">
          {renderLakes()}
          {renderHills()}
          <button className="add-grass-button" onClick={addGrass}>Add Grass</button>
          <button className="add-sheep-button" onClick={addSheep}>Add Sheep</button>
          <button className="add-wolf-button" onClick={addWolf}>Add Wolf</button>
          
          {/* Use the method "map" to traverse the array of grass and render a "Grass" component for each grass */}
          {grassAgent && grassAgent.map((grass) => (
            
            <Grass key={grass.id} grass={grass} style={getStyle(grass.grid_position)} />
))}
          {/* Use the method "map" to traverse the array of sheep and render a "Sheep" component for each sheep */}
          {moutonAgent && moutonAgent.map((sheep) => (
            <Sheep key={sheep.id} sheep={sheep} style={getStyle(sheep.grid_position)} />
          ))}
          {/* Use the method "map" to traverse the array of wolf and render a "Wolf" component for each wolf */}
          {loupAgent && loupAgent.map((wolf) => (
            <Wolf key={wolf.id} wolf={wolf} style={getStyle(wolf.grid_position)} />
          ))}
          <div className="grid-container">
            {cells}
          </div>
        </div>
        <div className="stats-container">
          <p>Day&nbsp; {step}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Weather &nbsp;{weather === 0 ? "Sunny" : "Rainy"} </p>
          <p></p>
          <p></p>
          <p></p>
          <p>Grass:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {grassCount}</p>
          <p>Sheep: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{sheepCount}</p>
          <p>Wolves: &nbsp;&nbsp;&nbsp;{wolfCount}</p>
          <p>Lakes: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {lakeCount}</p>
          <p>Hills:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;{hillCount}</p>
        </div>
      </div>
    );
    
};

 

  
function Sheep({ sheep }) {
  const controls = useAnimation();
  /*
  const dynamicStylesheep = {
    left: `${sheep.grid_position.x}px`,
    top: `${sheep.grid_position.y}px`,
  };
*/
  const ref = useRef(null);
  const initialStyle = getStyle(sheep.grid_position);
  // State variables for sheep properties
  const [age, setAge] = useState(sheep.age);
  const [sex, setSex] = useState(sheep.sex);
  const [id, setId] = useState(sheep.id);
  const [pregnant, setPregnant] = useState(sheep.pregnant);
  const [strength, setStrength] = useState(sheep.strength);

  const [showInfo, setShowInfo] = useState(false);
  const toggleInfo = () => {
    setShowInfo(!showInfo);
  };
  // Define hooks for animation and references
  

  // // Effect for position animation
  // useEffect(() => {
  //   controls.start({
  //     x: sheep.position.x * 10,
  //     y: sheep.position.y * 10,
  //   });
  // }, [sheep.position, controls]);

  // Effect for death animation
  useEffect(() => {
    if (sheep.isDead) {
      controls.start({ opacity: 0 });
    }
  }, [sheep.isDead, controls]);

  // Effect for position animation
  useEffect(() => {
    //const animationStylesheep = getStyle(sheep.grid_position);
    controls.start({
      x: initialStyle.left,
      y: initialStyle.top,
    });
  }, [sheep.grid_position, controls,SCALE]);


  // Effect for updating sheep information
  useEffect(() => {
    setAge(sheep.age);
    setSex(sheep.sex);
    setId(sheep.id);
    setPregnant(sheep.pregnant);
    setStrength(sheep.strength);
  }, [sheep]);

  // Dynamically calculate the size of an image
  const imageSize = 30 + (strength - 13) * 10;  
  
  const sheepImage = pregnant ? 'sheep_pregnant.png' : 'sheep1.png';

  return (
    <div className="agent sheep"  onClick={toggleInfo}>
      <motion.img
        ref={ref}
        src={sheepImage} // Replace with the actual image path
        alt="sheep"
        animate={controls} // Animation controller
        initial={{ x: initialStyle.left, y: initialStyle.top }}
        transition={{ duration: 0.35 }} // Animation transition properties
        style={{ width: imageSize + 'px', height: imageSize + 'px' }} 
      />
      {showInfo && (
      <div className="agent-info" style={{
        position: 'absolute', 
        left: initialStyle.left, 
        top: initialStyle.top,    
      }}>
        
        <p>Age: {age}</p>
        <p>Gender: {sex}</p>
        <p>ID: {id}</p>
        {pregnant ? <p>Pregnant</p> : <p>Unpregnant</p>}
        {strength > 17 ? <p>Full</p> : <p>Hungry</p>}
      </div>
      )}
    </div>
  );
}


// Define a Wolf component for rendering wolf and simulating wolf behavior and status
function Wolf({ wolf }) {
const controls = useAnimation();
/*
const dynamicStylewolf = {
  left: `${wolf.grid_position.x}px`,
  top: `${wolf.grid_position.y}px`,
};
*/
const ref = useRef(null);
const initialStyle = getStyle(wolf.grid_position);
const [age, setAge] = useState(wolf.age);
const [sex, setSex] = useState(wolf.sex);
const [id, setId] = useState(wolf.id);
const [pregnant, setPregnant] = useState(wolf.pregnant);
const [strength, setStrength] = useState(wolf.strength);

const [showInfo, setShowInfo] = useState(false);
  const toggleInfo = () => {
    setShowInfo(!showInfo); 
  };

// useEffect(() => {
//   controls.start({
//     x: wolf.position.x * 10,
//     y: wolf.position.y * 10,
//   });
// }, [wolf.position, controls]);

// Effect for position animation


useEffect(() => {
  //const animationStylesheep = getStyle(sheep.grid_position);
  controls.start({
    x: initialStyle.left,
    y: initialStyle.top,
  });
}, [wolf.grid_position, controls,SCALE]);

// Use the useEffect hook to listen for changes in the wolf's death state and trigger the disappearing animation.
useEffect(() => {
  if (wolf.isDead) {
    controls.start({
      opacity: 0,
    });
  }
}, [wolf.isDead, controls]);

// Use the useEffect hook to monitor changes in wolf information and update the status
useEffect(() => {
  setAge(wolf.age);
  setSex(wolf.sex);
  setId(wolf.id);
  setPregnant(wolf.pregnant);
  setStrength(wolf.strength);
}, [wolf]);

// Dynamically calculate the size of an image
const imageSize = 30 + (strength - 25) * 8;  

const wolfImage = pregnant ? 'wolf2.png' : 'wolf1.png';

return (
  <div className="agent wolf"  onClick={toggleInfo}>
    <motion.img
      ref={ref}
      src={wolfImage} // Replace with the actual image path
      alt="wolf"
      initial={{ x: initialStyle.left, y: initialStyle.top }}
      animate={controls} // Animation controller
      transition={{ duration: 0.35 }} // Animation transition properties
      style={{ width: imageSize + 'px', height: imageSize + 'px' }} 
    />
    {showInfo && (
      <div className="agent-info" style={{
        position: 'absolute', 
        left: initialStyle.left, 
        top: initialStyle.top,   
      }}>
      <p>Age: {age}</p>
      <p>Gender: {sex}</p>
      <p>ID: {id}</p>
      {pregnant ? <p>Pregnant</p> : <p>Unpregnant</p>}
      {strength > 28 ? <p>Full</p> : <p>Hungry</p>}
    </div>
    )}
  </div>
);
}



// Define a Grass component for rendering grass and simulating the behavior and status of grass
function Grass({ grass }) {
// Define a useAnimation hook to control the animation of the grass 
const controls = useAnimation();
const initialStyle = getStyle(grass.grid_position);
/*
const dynamicStylegrass = {
  left: `${grass.grid_position.x}px`,
  top: `${grass.grid_position.y}px`,
};
*/
// Define a useRef hook to obtain a reference to the grass image
const ref = useRef(null);
// Define some state variables to store grass information
const [age, setAge] = useState(grass.age);
const [id, setId] = useState(grass.id);

const [showInfo, setShowInfo] = useState(false);
  const toggleInfo = () => {
    setShowInfo(!showInfo); // Switch the display state of the information box
  };

// useEffect(() => {
//   controls.start({
//     x: grass.position.x * 10,
//     y: grass.position.y * 10,
//   });
// }, [grass.position, controls]);

// Effect for position animation

useEffect(() => {
  //const animationStylesheep = getStyle(sheep.grid_position);
  controls.start({
    x: grass.grid_position.X * SCALE,
    y: grass.grid_position.Y * SCALE,
  });
}, [grass.grid_position, controls,SCALE]);

// Use the useEffect hook to listen for changes in the grass's death state and trigger the disappearing animation.
useEffect(() => {
  if (grass.isDead) {
    controls.start({
      opacity: 0,
    });
  }
}, [grass.isDead, controls]);

// Use the useEffect hook to monitor changes in grass information and update the status
useEffect(() => {
  setAge(grass.age);
  setId(grass.id);
}, [grass]);

return (
  <div className="agent grass" style={initialStyle}  onClick={toggleInfo}>
    <motion.img
      ref={ref}
      src="grass1.jpg" // Replace with the actual image path
      alt="grass"
      //animate={controls}
      //transition={{ duration: 1 }}
    />
    {showInfo && (
      <div className="agent-info" style={{
        position: 'absolute', 
        left: '100%', 
        top: '0',    
      }}>
      <p>Age: {age}</p>
      <p>ID: {id}</p>
    </div>
    )}
  </div>
);
}


export default App;