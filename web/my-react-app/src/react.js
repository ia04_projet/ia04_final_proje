import React, { useState, useEffect } from "react";
import axios from "axios";
import { motion } from "framer-motion";



function App() {
  // 定义一些状态变量，用于存储API返回的数据
  const [sheepCount, setSheepCount] = useState(0);
  const [wolfCount, setWolfCount] = useState(0);
  const [grassCount, setGrassCount] = useState(0);
  const [grassAgents, setGrassAgents] = useState([]);
  const [sheepAgents, setSheepAgents] = useState([]);
  const [wolfAgents, setWolfAgents] = useState([]);

  // 定义一个函数，用于发送GET请求到API
  const fetchSimulationStats = async () => {
    try {
      const response = await axios.get("/ws");
      const data = response.data;
      // 更新状态变量
      setSheepCount(data.sheepCount);
      setWolfCount(data.wolfCount);
      setGrassCount(data.grassCount);
      setGrassAgents(data.grassAgents);
      setSheepAgents(data.moutonAgents);
      setWolfAgents(data.loupAgents);
    } catch (error) {
      console.error(error);
    }
  };

  // 使用useEffect钩子，在组件挂载时发送一次GET请求，然后每隔一秒发送一次
  useEffect(() => {
    fetchSimulationStats();
    const interval = setInterval(() => {
      fetchSimulationStats();
    }, 1000);
    // 在组件卸载时清除定时器
    return () => {
      clearInterval(interval);
    };
  }, []);

  // 定义一个函数，用于根据位置坐标计算CSS样式
  const getStyle = (position) => {
    return {
      left: `${position.x * 10}px`,
      top: `${position.y * 10}px`,
    };
  };

  return (
    <div className="App">
      <h1>生态系统模拟</h1>
      <p>羊的数量：{sheepCount}</p>
      <p>狼的数量：{wolfCount}</p>
      <p>草的数量：{grassCount}</p>
      <div className="container">
        {/* 使用map方法遍历草的数组，为每个草渲染一个Grass组件 */}
        {grassAgents.map((grass) => (
          <Grass key={grass.id} grass={grass} />
        ))}
        {/* 使用map方法遍历羊的数组，为每个羊渲染一个Sheep组件 */}
        {sheepAgents.map((sheep) => (
          <Sheep key={sheep.id} sheep={sheep} />
        ))}
        {/* 使用map方法遍历狼的数组，为每个狼渲染一个Wolf组件 */}
        {wolfAgents.map((wolf) => (
          <Wolf key={wolf.id} wolf={wolf} />
        ))}
      </div>
    </div>
  );
}

// 定义一个Sheep组件，用于渲染羊的贴图和模拟羊的行为和状态
function Sheep({ sheep }) {
  // 定义一个useAnimation钩子，用于控制羊的贴图的动画
  const controls = useAnimation();
  // 定义一个useRef钩子，用于获取羊的贴图的引用
  const ref = useRef(null);
  // 定义一些状态变量，用于存储羊的信息
  const [age, setAge] = useState(sheep.age);
  const [sex, setSex] = useState(sheep.sex);
  const [id, setId] = useState(sheep.id);
  const [pregnant, setPregnant] = useState(sheep.pregnant);
  const [strength, setStrength] = useState(sheep.strength);

  // 使用useEffect钩子，监听羊的位置的变化，并触发移动的动画
  useEffect(() => {
    controls.start({
      x: sheep.position.x * 10,
      y: sheep.position.y * 10,
    });
  }, [sheep.position, controls]);

  // 使用useEffect钩子，监听羊的死亡状态的变化，并触发消失的动画
  useEffect(() => {
    if (sheep.isDead) {
      controls.start({
        opacity: 0,
      });
    }
  }, [sheep.isDead, controls]);

  // 使用useEffect钩子，监听羊的信息的变化，并更新状态
  useEffect(() => {
    setAge(sheep.age);
    setSex(sheep.sex);
    setId(sheep.id);
    setPregnant(sheep.pregnant);
    setStrength(sheep.strength);
  }, [sheep]);

  return (
    <motion.img
      ref={ref}
      src="C:\Users\12161\Desktop\sheep.jpg" // 本地图片的路径
      alt="sheep"
      className="sheep" // CSS类名
      style={getStyle(sheep.position)} // CSS样式
      animate={controls} // 动画控制器
      transition={{ duration: 1 }} // 动画过渡属性，设置动画持续时间为1秒
    >
      {/* 在羊的贴图下方渲染一个div元素，用于显示羊的信息 */}
      <div className="display-inline-block bg-white p-2 m-1 rounded">
        <p>年龄：{age}</p>
        <p>性别：{sex}</p>
        <p>ID:{id}</p>
        {/* 根据羊的怀孕状态显示不同的内容 */}
        {pregnant ? <p>怀孕中</p> : <p>未怀孕</p>}
        {/* 根据羊的饱腹状态显示不同的内容 */}
        {strength > 15 ? <p>饱了</p> : <p>饿了</p>}
      </div>
    </motion.img>
  );
}

// 定义一个Wolf组件，用于渲染狼的贴图和模拟狼的行为和状态
function Wolf({ wolf }) {
  // 定义一个useAnimation钩子，用于控制狼的贴图的动画
  const controls = useAnimation();
  // 定义一个useRef钩子，用于获取狼的贴图的引用
  const ref = useRef(null);
  // 定义一些状态变量，用于存储狼的信息
  const [age, setAge] = useState(wolf.age);
  const [sex, setSex] = useState(wolf.sex);
  const [id, setId] = useState(wolf.id);
  const [pregnant, setPregnant] = useState(wolf.pregnant);
  const [strength, setStrength] = useState(wolf.strength);

  // 使用useEffect钩子，监听狼的位置的变化，并触发移动的动画
  useEffect(() => {
    controls.start({
      x: wolf.position.x * 10,
      y: wolf.position.y * 10,
    });
  }, [wolf.position, controls]);

  // 使用useEffect钩子，监听狼的死亡状态的变化，并触发消失的动画
  useEffect(() => {
    if (wolf.isDead) {
      controls.start({
        opacity: 0,
      });
    }
  }, [wolf.isDead, controls]);

  // 使用useEffect钩子，监听狼的信息的变化，并更新状态
  useEffect(() => {
    setAge(wolf.age);
    setSex(wolf.sex);
    setId(wolf.id);
    setPregnant(wolf.pregnant);
    setStrength(wolf.strength);
  }, [wolf]);

  return (
    <motion.img
      ref={ref}
      src="C:\Users\12161\Desktop\wolf.jpg" // 本地图片的路径
      alt="wolf"
      className="wolf" // CSS类名
      style={getStyle(wolf.position)} // CSS样式
      animate={controls} // 动画控制器
      transition={{ duration: 1 }} // 动画过渡属性，设置动画持续时间为1秒
    >
      {/* 在狼的贴图下方渲染一个div元素，用于显示狼的信息 */}
      <div className="display-inline-block bg-white p-2 m-1 rounded">
        <p>年龄：{age}</p>
        <p>性别：{sex}</p>
        <p>ID:{id}</p>
        {/* 根据狼的怀孕状态显示不同的内容 */}
        {pregnant ? <p>怀孕中</p> : <p>未怀孕</p>}
        {/* 根据狼的饱腹状态显示不同的内容 */}
        {strength > 30 ? <p>饱了</p> : <p>饿了</p>}
      </div>
    </motion.img>
  );
}


// 定义一个Grass组件，用于渲染草的贴图和模拟草的行为和状态
function Grass({ grass }) {
  // 定义一个useAnimation钩子，用于控制草的贴图的动画
  const controls = useAnimation();
  // 定义一个useRef钩子，用于获取草的贴图的引用
  const ref = useRef(null);
  // 定义一些状态变量，用于存储草的信息
  const [age, setAge] = useState(grass.age);
  const [id, setId] = useState(grass.id);

  // 使用useEffect钩子，监听草的位置的变化，并触发移动的动画
  useEffect(() => {
    controls.start({
      x: grass.position.x * 10,
      y: grass.position.y * 10,
    });
  }, [grass.position, controls]);

  // 使用useEffect钩子，监听草的死亡状态的变化，并触发消失的动画
  useEffect(() => {
    if (grass.isDead) {
      controls.start({
        opacity: 0,
      });
    }
  }, [grass.isDead, controls]);

  // 使用useEffect钩子，监听草的信息的变化，并更新状态
  useEffect(() => {
    setAge(grass.age);
    setId(grass.id);
  }, [grass]);

  return (
    <motion.img
      ref={ref}
      src="C:\Users\12161\Desktop\grass.jpg" // 本地图片的路径
      alt="grass"
      className="grass" // CSS类名
      style={getStyle(grass.position)} // CSS样式
      animate={controls} // 动画控制器
      transition={{ duration: 1 }} // 动画过渡属性，设置动画持续时间为1秒
    >
      {/* 在草的贴图下方渲染一个div元素，用于显示草的信息 */}
      <div className="display-inline-block bg-white p-2 m-1 rounded">
        <p>年龄：{age}</p>
        <p>ID:{id}</p>
      </div>
    </motion.img>
  );
}


export default App;
